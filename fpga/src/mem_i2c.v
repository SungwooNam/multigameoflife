/*
address region
    0x0000 : I2C WORD HI, LO
    0x0004 : I2C FIFO status
        0[31:29] wfifo_full[28] 
        0[27:25] wfifo_empty[24]
        0[23:21] rfifo_full[20]
        0[19:17] rfifo_empty[16]
        0[15:0]
*/

`timescale 1ns / 1ps

module mem_i2c 
    #(  
        parameter integer SCL_PERIOD_CLOCKS = 4,
        parameter integer WRITE_FIFO_DEPTH = 256,
        parameter integer READ_FIFO_DEPTH = 8,
        parameter integer USE_BLOCK_RAM_FIFO = 0
    )
    (
        input clk,
        input rst_n,

        input sda_i,
        output sda_o,
        output sda_t,
		output scl,

        input [15:0] addr,
        input [31:0] wdata,
        input wstrb,
        output reg wbusy,
        output [31:0] rdata,
        input rstrb,
        output reg rbusy
    );

    reg [15:0] fifo_wdata;
    reg fifo_wren;	
    wire fifo_full;	

    wire [15:0] fifo_rdata;
    reg fifo_rden;
    wire fifo_empty;
	
	wire cmd_rden;
	wire [15:0] cmd_rdata;
	wire cmd_rempty;
	
	wire rsp_wren;
	wire [15:0] rsp_wdata;
	wire rsp_wfull;

    localparam 
        IDLE =  2'h0,
        PUSH_WORD =  2'h1,
        CLEAN_UP =  2'h2,
        PANIC = 2'h3;

    reg [1:0] state_push;

    always @(posedge clk) begin
		if (rst_n == 0 ) begin
			fifo_wren <= 0;
            wbusy <= 0;
            state_push <= IDLE;
		end
		else begin 
            
            case( state_push )
                IDLE : begin
                    if( wstrb == 1 ) begin
                        fifo_wren <= 1;
                        fifo_wdata <= wdata[31:16];
                        wbusy <= 1;
                        state_push <= PUSH_WORD;
                    end
                end

                PUSH_WORD : begin
                    fifo_wdata <= wdata[15:0];
                    state_push <= CLEAN_UP;
                end

                CLEAN_UP : begin
                    fifo_wren <= 0;
                    wbusy <= 0;
                    state_push <= IDLE;
                end
            endcase
        end
	end

    always @(posedge clk) begin
		if (rst_n == 0 ) begin
			fifo_rden <= 0;
            rbusy <= 0;
		end
	end

    assign rdata = { 
        3'h0, fifo_full, 
        3'h0, cmd_rempty, 
        3'h0, rsp_wfull, 
        3'h0, fifo_empty,
        16'h0 };

    fifo_ex #(
        .DATA_WIDTH(16),
        .FIFO_DEPTH(WRITE_FIFO_DEPTH),
        .USE_BLOCK_RAM( USE_BLOCK_RAM_FIFO ),
        .ALMOST_BITS(1)
    ) 
        cmd_fifo(
            .clk(clk), 
            .rst_n(rst_n),

            .wren_i(fifo_wren),
            .wdata_i(fifo_wdata),
            .almost_full_o(fifo_full),

            .rden_i(cmd_rden),
            .rdata_o(cmd_rdata),
            .empty_o(cmd_rempty)
        );

    fifo_ex #(
        .DATA_WIDTH(16),
        .FIFO_DEPTH(READ_FIFO_DEPTH),
        .ALMOST_BITS(1)
    ) 
        rsp_fifo(
            .clk(clk), 
            .rst_n(rst_n),

            .wren_i(rsp_wren),
            .wdata_i(rsp_wdata),
            .almost_full_o(rsp_wfull),

            .rden_i(fifo_rden),
            .rdata_o(fifo_rdata),
            .empty_o(fifo_empty)
        );  

	i2c_master #(  
        .SCL_PERIOD_CLOCKS(SCL_PERIOD_CLOCKS),
		.DELAY_BITS_AT_STOP(2)
	) 
		i2c (
			.clk(clk),
			.rst_n(rst_n),

			.sda_i(sda_i),
            .sda_o(sda_o),
            .sda_t(sda_t),

			.scl(scl),

			.rden(cmd_rden),
			.rdata(cmd_rdata),
			.rempty(cmd_rempty),

			.wren(rsp_wren),
			.wdata(rsp_wdata),
			.wfull(rsp_wfull)
		);

endmodule
