#!/bin/bash
SOURCE_PATH=$(pwd)
BUILD_PATH=../../../../workspace_femto_vitis
XSA_PATH==../../../../workspace_femto_vivado/minized_femto.xsa
TARGET_NAME=minized_femto

SHORT=b:,x:,t:,h
LONG=build_path:,xsa_path:,target_name:,help
OPTS=$(getopt --alternative --name build_vitis --options $SHORT --longoptions $LONG -- "$@") 

eval set -- "$OPTS"

while :
do
  case "$1" in
    -b | --build_path )
      BUILD_PATH="$2"
      shift 2
      ;;
    -x | --xsa_path )
      XSA_PATH="$2"
      shift 2
      ;;
    -t | --target_name )
      TARGET_NAME="$2"
      shift 2
      ;;
    -h | --help)
      "vitis build"
      exit 2
      ;;
    --)
      shift;
      break
      ;;
    *)
      echo "Unexpected option: $1"
      ;;
  esac
done

source /opt/Xilinx/Vivado/2020.2/settings64.sh
PATH=$PATH:/opt/Xilinx/Vitis/2020.2/bin

mkdir -p $BUILD_PATH
rm -rf $BUILD_PATH/.metadata
rm -rf $BUILD_PATH/.Xil
rm -rf $BUILD_PATH/minized_*
rm -rf $BUILD_PATH/femto_app*
rm -rf $BUILD_PATH/sd_card

cp $XSA_PATH $BUILD_PATH
cp -r -f ./src $BUILD_PATH
# cp build_vitis.tcl $BUILD_PATH

cd $BUILD_PATH
tee build_vitis.tcl << BUILD_VITIS_TCL
#
# execute this on a build path not source path otherwise the path will be populated with temporary files
#

setws .
#app remove femto_app

app create -name femto_app -hw ./$TARGET_NAME.xsa -proc ps7_cortexa9_0 -os standalone -lang c++ -template "Empty Application (C++)"
importsource -name femto_app -path ./src 
app build -name femto_app
BUILD_VITIS_TCL
xsct build_vitis.tcl

sdcard_gen \
    --xpfm ./$TARGET_NAME/export/$TARGET_NAME/$TARGET_NAME.xpfm \
    --sys_config $TARGET_NAME \
    --bif ./$TARGET_NAME/export/$TARGET_NAME/sw/$TARGET_NAME/boot/$TARGET_NAME.bif \
    --bitstream $TARGET_NAME.bit \
    --elf ./femto_app/Debug/femto_app.elf,ps7_cortexa9_0 

cp ./$TARGET_NAME/zynq_fsbl/fsbl.elf ./sd_card
cp $SOURCE_PATH/program.sh ./sd_card
