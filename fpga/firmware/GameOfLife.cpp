#include "SSD1306.hpp"
#include "FrameBuffer.hpp"
#include "LinearFeedbackShiftRegister.hpp"

int main()
{
    using namespace RISCV;

    Memory<AllocBareMetal> mem;
    SSD1306 sd( mem );

    {
        uint8_t init[] = { 0x8d, 0x14, 0xa4, 0xaf, 0x20, 0x00, 0x20, 0x00 };
        sd.Command( init, 8);
        uint8_t init_region[] = { 0x21, 0x00, 0x7f, 0x22, 0x00, 0x07, 0x20, 0x00 };
        sd.Command( init_region, 8);
    }

    uint16_t seed = mem.GetParam<uint16_t>(0);
    if( seed == 0 ) {
        seed = 0xCAFE;
    }

    XORShift lfsr( seed );

    FrameBuffer fb1( mem, 0, 128, 64 );
    FrameBuffer fb2( mem, 1024, 128, 64 );

    for(;;)
    {
        uint32_t prev_change_count = 0;

        fb1.Clear( 0x00000000 );        
        // sd.DataStream( reinterpret_cast<uint8_t*>(MemoryRegion::HEAP_START + fb1.GetBaseAddr() ), 1024, 1e7 );

        for( uint32_t i = 0; i < fb1.width*fb1.height/4; ++i )
        {
            fb1.SetPixel( lfsr.Random()%fb1.width, lfsr.Random()%fb1.height, 1 );
        }
        sd.DataStream( reinterpret_cast<uint8_t*>(MemoryRegion::HEAP_START + fb1.GetBaseAddr() ), 1024 );

        FrameBuffer<>* present = &fb1;
        FrameBuffer<>* next = &fb2;
        
        for( ;; )
        {
            uint32_t change_count = 0;
    
            for ( uint8_t y =0; y < present->height; ++y ) {
                for( uint8_t x = 0; x < present->width; ++x ) {
                    uint8_t live = present->GetPixel( x, y);
                    uint8_t nc = present->GetNeighbor( x, y);
                    if( live == 1) {
                        if( nc == 2 || nc == 3 ) 
                        {
                            next->SetPixel( x, y, 1 ); 
                        }
                        else 
                        {
                            next->SetPixel( x, y, 0 );
                            change_count++;
                        }
                    }
                    else
                    {
                        if( nc == 3 ) 
                        {
                            next->SetPixel( x, y, 1 ); 
                            change_count++;
                        }
                        else 
                        {
                            next->SetPixel( x, y, 0 ); 
                        }
                    }
                }
            }

            sd.DataStream( reinterpret_cast<uint8_t*>(MemoryRegion::HEAP_START + next->GetBaseAddr() ), 1024 );

            FrameBuffer<>* temp = next;
            next = present;
            present = temp;

            if( prev_change_count == change_count && change_count < fb1.width*fb1.height/50)
            {
                break;
            }
            else 
            {
                prev_change_count = change_count;
            }
        }

    }

    while(1);
}
