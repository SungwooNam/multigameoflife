// lui 
function [31:0] asm_lui( input [19:0] imm_const, input [4:0] reg_dest ); 
    asm_lui = { imm_const, reg_dest, 7'b0110111 };
endfunction

function [31:0] asm_sw( input [11:0] imm_addr_offset, input [4:0] reg_value, input [4:0] reg_addr ); 
    asm_sw = { imm_addr_offset[11:5], reg_value, reg_addr, 3'b010, imm_addr_offset[4:0], 7'b0100011 };
endfunction

function [31:0] asm_sh( input [11:0] imm_addr_offset, input [4:0] reg_value, input [4:0] reg_addr ); 
    asm_sh = { imm_addr_offset[11:5], reg_value, reg_addr, 3'b001, imm_addr_offset[4:0], 7'b0100011 };
endfunction

function [31:0] asm_sb( input [11:0] imm_addr_offset, input [4:0] reg_value, input [4:0] reg_addr ); 
    asm_sb = { imm_addr_offset[11:5], reg_value, reg_addr, 3'b000, imm_addr_offset[4:0], 7'b0100011 };
endfunction

function [31:0] asm_addi( input [11:0] imm_const, input [4:0] reg_value, input [4:0] reg_dest ); 
    asm_addi = { imm_const, reg_value, 3'b000, reg_dest, 7'b0010011 };
endfunction

function [31:0] asm_lw( input [11:0] imm_offset, input [4:0] reg_addr, input [4:0] reg_dest ); 
    asm_lw = { imm_offset, reg_addr, 3'b010, reg_dest, 7'b0000011 };
endfunction

function [31:0] asm_loop( input[15:0] addr ); 
    asm_loop = 32'h0000_006f;
endfunction

function [31:0] asm_fadds( input[4:0] rs1, input[4:0] rs2, input[4:0] rd, input[2:0] rm ); 
    asm_fadds = { 8'b00000000, rs2, rs1, rm, rd, 7'b1010011 };
endfunction
