#include <stdio.h>
#include "platform.h"
#include "xil_types.h"
#include "xstatus.h"
#include "xllfifo.h"
#include "xparameters.h"
#include "sleep.h"
#include "xuartps.h"
#include <string>
#include <vector>
#include <stdexcept>

#ifdef XPAR_AXI_FIFO_MM_S_0_DEVICE_ID
	#define AXI_FIFO_0_ID 		XPAR_AXI_FIFO_MM_S_0_DEVICE_ID
	#define AXI_FIFO_0_BASEADDR XPAR_AXI_FIFO_MM_S_0_BASEADDR
#endif

#ifdef XPAR_AXI_FIFO_0_DEVICE_ID
	#define AXI_FIFO_0_ID 		XPAR_AXI_FIFO_0_DEVICE_ID
	#define AXI_FIFO_0_BASEADDR XPAR_AXI_FIFO_0_BASEADDR
#endif

class Console {
private:
	XUartPs m_Handle;

public:
	Console( uint16_t devID, uint32_t baudrate )
	{
		XUartPs_Config *cfg = XUartPs_LookupConfig(devID);
		if( XUartPs_CfgInitialize(&m_Handle, cfg, cfg->BaseAddress) != XST_SUCCESS
			|| XUartPs_SetBaudRate(&m_Handle, baudrate) != XST_SUCCESS )
		{
			throw std::runtime_error( "failed to initialize");
		}
	}

	void Write( const char *format, ... )
	{
		char buf[128];
		va_list args;
		va_start(args, format);
		int len = vsnprintf(buf, sizeof(buf), format, args);
		va_end(args);

		for ( int sent = 0; sent < len; ) {
			sent += XUartPs_Send(&m_Handle, (u8*)(buf+sent), len);
		}
	}

	std::string ReadLine()
	{
		char buf[512];
		size_t len = 0;
		while(1) {
			int recv = XUartPs_Recv(&m_Handle, (u8*)(buf+len), sizeof(buf)-len);
			for( size_t i=0; i<(size_t)recv; ++i, ++len ) {
				if ( buf[len] == '\r' || len >= sizeof(buf)) {
					return std::string(buf, buf+len);
				}
			}
		}

		throw std::runtime_error("No Way");
	}
};

class AxiFifo
{
private:
	XLlFifo m_Handle;

public:
	AxiFifo( uint32_t devid, UINTPTR addr )
	{
	    if( XLlFifo_CfgInitialize( &m_Handle, XLlFfio_LookupConfig(devid), addr)
	    	!= XST_SUCCESS ) {
	        throw std::runtime_error( "Failed to initialize FIFO");
	    }

	    XLlFifo_IntClear(&m_Handle,0xffffffff);
	    if(XLlFifo_Status(&m_Handle) != 0x0) {
	        throw std::runtime_error( "Reset value wrong");
	    }
	}

	void Write( std::vector<uint32_t> values )
	{
        for( auto v : values ) { XLlFifo_TxPutWord(&m_Handle, v); }
        XLlFifo_iTxSetLen(&m_Handle, sizeof(uint32_t)*values.size() );
	}

	std::vector<uint32_t> Read()
	{
		std::vector<uint32_t> v;
        for( int count = XLlFifo_iRxOccupancy(&m_Handle); count>0; count--) {
        	v.push_back( XLlFifo_RxGetWord(&m_Handle) );
        }
        return v;
	}
};

std::vector<std::string> tokenize(const std::string& msg, const std::string& delimiter)
{
	std::vector<std::string> tokens;
    size_t start = msg.find_first_not_of(delimiter), end=start;
    while (start != std::string::npos){
        end = msg.find_first_of(delimiter, start);
        tokens.push_back(msg.substr(start, end-start));
        start = msg.find_first_not_of(delimiter, end);
    }
    return tokens;
}

int main()
{
    init_platform();

    Console console( XPAR_PS7_UART_1_DEVICE_ID, 115200);
	console.Write( "femto 0.3.1\r\n> " );

    try
    {
        AxiFifo fifo( AXI_FIFO_0_ID, AXI_FIFO_0_BASEADDR);

        for(;;) {
        	console.Write( "\r\n> " );

        	auto tokens = tokenize( console.ReadLine(), " \r\n");
            if( tokens.size() == 0 ) continue;

            if( tokens[0] == "push")
            {
            	std::vector<uint32_t> words;
            	for( size_t i = 1; i < tokens.size(); ++i) {
            		words.push_back( strtoul(tokens[i].c_str(), nullptr,16));
            	}
            	fifo.Write( words );
            	console.Write("ok");
            }
            else if ( tokens[0] == "pop")
            {
            	for( auto v : fifo.Read()) { console.Write("%08x ",v); }
            }
            else
            {
            	console.Write("unknown command : %s", tokens[0].c_str());
            }
        }
    }
    catch( std::exception& ex )
    {
    	console.Write( "exception caught : %s", ex.what());
    }

    cleanup_platform();
    return 0;
}
