`timescale 1ns / 1ps

// block ram with 32bits per address
// read/write takes 1 clock. 
module bram_32bits
    #( 
        parameter INIT = "none",  
        parameter integer MEM_SIZE_IN_BYTES = 64,    
        parameter integer ADDR_WIDTH = 16 
    )
    (
        input clk, 

        input [ADDR_WIDTH-1:0] waddr,
        input [31:0] wdata,
        input wenable,

        input [ADDR_WIDTH-1:0] raddr,
        output [31:0] rdata
    );
    
    reg [31:0] mem[0:MEM_SIZE_IN_BYTES/4-1];
    reg [ADDR_WIDTH-1:0] raddr_reg;
    
	always @(posedge clk) begin
	    raddr_reg <= raddr;
        if (wenable != 0) begin
            mem[waddr] <= wdata;
        end
    end
    assign rdata = mem[raddr_reg];    

    if (INIT != "none") begin
        initial begin
            $readmemh( INIT, mem );
        end
    end

endmodule
