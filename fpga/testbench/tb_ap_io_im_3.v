`include "testbench.v"

`timescale 1ns / 1ps

module tb_ap_io_im_3;
	`include "../src/asm.v"
	`include "../src/cmd.v"

    reg clk, rst, sw_rst;

    reg [31:0] cmd_data;
    reg cmd_valid;
    wire cmd_ready;

    wire [31:0] resp_data;
    wire resp_valid;
    wire resp_last;
    reg resp_ready;

    wire [23:0] mem_addr;
    wire mem_wstrb;
    wire mem_wready;
    wire [31:0] mem_wdata;

    cmd_decoder #( )
        uut_cmd_decoder( 
            .clk(clk), .rst_n(~rst), 
            .rx_data( cmd_data ),
            .rx_valid( cmd_valid ),
            .rx_ready( cmd_ready ), 
            .tx_data( resp_data), 
            .tx_valid( resp_valid ), 
            .tx_last( resp_last ), 
            .tx_ready( resp_ready ),
            .mem_addr( mem_addr ),
            .mem_wstrb( mem_wstrb ),
            .mem_wready( mem_wready ),
            .mem_wdata( mem_wdata ) 
        );
            
    wire [15:0] io_addr;
    wire [31:0] io_wdata;
    wire io_wstrb;
    reg [31:0] io_rdata;
    wire io_rstrb;
    reg io_wbusy;
    reg io_rbusy;

    array_processor_io_im #(
            .PROCESSOR_COUNT(1),
            .INSTRUCTION_MEM_SIZE(4096),
            .PARAM_MEM_SIZE(32),
            .HEAP_MEM_SIZE(4096)
        )
        ap(
            .clk(clk), .rst(rst),
            .sw_rst(sw_rst),
            .prog_addr( mem_addr ),
            .prog_wdata( mem_wdata ),
            .prog_wstrb( mem_wstrb ),
            .prog_wready( mem_wready ),

            .io_addr_0( io_addr ),
            .io_wdata_0( io_wdata ),
            .io_wstrb_0( io_wstrb ),
            .io_rdata_0( io_rdata ),
            .io_rstrb_0( io_rstrb ),
            .io_wbusy_0( io_wbusy ),
            .io_rbusy_0( io_rbusy )
        );

/*
    mem.SetHeap( 0, 0x12345678 );
    uint8_t v = mem.GetHeap<uint8_t>( 2 );
    mem.SetHeap( 1, v );
    mem.SetIO( 0, mem.GetHeap<uint32_t>(0) );

   c:   002007b7                lui     a5,0x200
  10:   12345737                lui     a4,0x12345
  14:   67870713                addi    a4,a4,1656 # 12345678 <SP_START+0x12143678>
  18:   00e7a023                sw      a4,0(a5) # 200000 <__global_pointer$+0x1fe7a0>
  1c:   0027c703                lbu     a4,2(a5)
  20:   00e780a3                sb      a4,1(a5)
  24:   0007a703                lw      a4,0(a5)
  28:   003007b7                lui     a5,0x300
  2c:   00e7a023                sw      a4,0(a5) # 300000 <SP_START+0xfe000>
*/

    initial begin
        sw_rst = 0; rst = 1;
        io_wbusy = 0; io_rbusy = 0; io_rdata = 0;

        #10 rst = 0; resp_ready = 1; cmd_valid=0; 
        @(posedge clk);
        @(posedge clk) #1 cmd_valid=1; cmd_data=cmd_io( 0 );  
        @(posedge cmd_ready) #1 cmd_data=cmd_mem_write(24'h01_0000, 9);  
        @(posedge cmd_ready) #1 cmd_data=32'h002007b7;  
        @(posedge cmd_ready) #1 cmd_data=32'h12345737;  
        @(posedge cmd_ready) #1 cmd_data=32'h67870713;  
        @(posedge cmd_ready) #1 cmd_data=32'h00e7a023;  
        @(posedge cmd_ready) #1 cmd_data=32'h0027c703;  
        @(posedge cmd_ready) #1 cmd_data=32'h00e780a3;  
        @(posedge cmd_ready) #1 cmd_data=32'h0007a703;  
        @(posedge cmd_ready) #1 cmd_data=32'h003007b7;  
        @(posedge cmd_ready) #1 cmd_data=32'h00e7a023;  
        @(posedge cmd_ready) #1 cmd_data=asm_loop( 0 );
        @(posedge cmd_ready) #1 cmd_valid=0; 
        @(posedge clk); 
        @(posedge clk); sw_rst = 1;        
    end

    initial begin
        clk = 1'b1;

        #2000 $finish;
    end

    always clk = #5 ~clk;

    initial begin
        $dumpfile("tb_ap_io_im_3.vcd");
        $dumpvars(-1, tb_ap_io_im_3); 
    end

    initial fork
        #735  `EXPECT_EQ( io_wdata, 32'h1234_3478);
    join

endmodule
