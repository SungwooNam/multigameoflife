`include "testbench.v"

`timescale 1ns / 1ps

module tb_i2c_4;
    reg clk, rst;

    wire sda_o;
    wire scl;
    wire cmd_rden;
    wire [15:0] cmd_rdata;
    wire cmd_rempty;
    wire rsp_wden;
    wire [15:0] rsp_wdata;
    wire rsp_wfull;
    reg rsp_rden;
    wire [15:0] rsp_rdata;
    wire rsp_rempty;
    reg [7:0] read_sda;
    reg sda_simul;

    i2c_master #( 
        .SCL_PERIOD_CLOCKS(8)
        )
        i2c( 
            .clk(clk), 
            .rst_n(~rst), 
            
            .sda_i( sda_simul ),
            .sda_o( sda_o ),
            .scl( scl ),

            .rden( cmd_rden ),
            .rdata( cmd_rdata ),
            .rempty( cmd_rempty ),

            .wren( rsp_wren ),
            .wdata( rsp_wdata ),
            .wfull( rsp_wfull )
        );

    reg cmd_wren;
    reg [15:0] cmd_wdata;
                    
    fifo_ex #(
        .DATA_WIDTH(16),
        .FIFO_DEPTH(16),
        .ALMOST_BITS(1)
    ) 
        cmd_fifo(
            .clk(clk), 
            .rst_n(~rst),

            .wren_i(cmd_wren),
            .wdata_i(cmd_wdata),

            .rden_i(cmd_rden),
            .rdata_o(cmd_rdata),
            .empty_o(cmd_rempty)
        );

    fifo_ex #(
        .DATA_WIDTH(16),
        .FIFO_DEPTH(8),
        .ALMOST_BITS(1)
    ) 
        rsp_fifo(
            .clk(clk), 
            .rst_n(~rst),

            .wren_i(rsp_wren),
            .wdata_i(rsp_wdata),
            .almost_full_o(rsp_wfull),

            .rden_i(rsp_rden),
            .rdata_o(rsp_rdata),
            .empty_o(rsp_rempty)
        );        

    initial begin
        cmd_wren=0; 
        rsp_rden=0;

        #5 rst = 1'b1;
        #10 rst = 1'b0;

        @(posedge clk);
        @(posedge clk); #1 cmd_wren=1; cmd_wdata=16'h12C2;
        @(posedge clk); #1 cmd_wren=1; cmd_wdata=16'hCC41;
        @(posedge clk); #1 cmd_wren=0;

        @(posedge clk);
        @(posedge clk); #1 cmd_wren=1; cmd_wdata=16'h13C3;
        @(posedge clk); #1 cmd_wren=1; cmd_wdata=16'hBEEF;
        @(posedge clk); #1 cmd_wren=1; cmd_wdata=16'hCAFE;
        @(posedge clk); #1 cmd_wren=0;

    end     


    initial begin
        clk = 1'b1;
        #8000 $finish;
    end

    always clk = #5 ~clk;

    initial begin
        $dumpfile("tb_i2c_4.vcd");
        $dumpvars(-1, tb_i2c_4); 
    end

    initial begin 
        
        @(negedge sda_o) #1 sda_simul = 1; 
        read_sda_at_rising_scl( 8 );
        `EXPECT_EQ( read_sda[7:1], 7'h42);
        `EXPECT_EQ( read_sda[0], 1'h0 );
        @(negedge scl) #1 sda_simul = 0;
        read_sda_at_rising_scl( 1 );

        @(negedge scl) #1 sda_simul = 1; 
        read_sda_at_rising_scl( 8 );
        `EXPECT_EQ( read_sda[7:0], 8'h41);
        @(negedge scl) #1 sda_simul = 0;
        read_sda_at_rising_scl( 2 );

        @(negedge sda_o) #1 sda_simul = 1; 
        read_sda_at_rising_scl( 8 );
        `EXPECT_EQ( read_sda[7:1], 8'h43);
        @(negedge scl) #1 sda_simul = 0;
        read_sda_at_rising_scl( 1 );

        @(negedge scl) #1 sda_simul = 1; 
        read_sda_at_rising_scl( 8 );
        `EXPECT_EQ( read_sda[7:0], 8'hEF);
        @(negedge scl) #1 sda_simul = 0;
        read_sda_at_rising_scl( 1 );

        @(negedge scl) #1 sda_simul = 1; 
        read_sda_at_rising_scl( 8 );
        `EXPECT_EQ( read_sda[7:0], 8'hCA);
        @(negedge scl) #1 sda_simul = 0;
        read_sda_at_rising_scl( 2 );

    end

    `include "tb_i2c_tasks.v"

endmodule
