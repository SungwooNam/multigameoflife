#ifndef __MEMORY_HPP__
#define __MEMORY_HPP__

#include "array_processor.h"

namespace RISCV {

struct AllocBareMetal {
    static void* alloc( uint32_t start, uint32_t sizeByte ) { return reinterpret_cast<void*>(start); } 
    static void free( void *region ) { } 
    static void on_read( uint32_t addr ) {}
    template<typename T> static void on_write( uint32_t addr, T value ) {}
};

struct AllocSTL {
    static void* alloc( uint32_t start, uint32_t sizeByte ) { return new uint32_t[sizeByte/4]; } 
    static void free( void *region ) { delete[] reinterpret_cast<uint32_t*>(region); } 
    static void on_read( uint32_t addr ) {}
    template<typename T> static void on_write( uint32_t addr, T value ) {}
};


template< typename TAlloc = AllocBareMetal >
class Memory
{
public:
    Memory()
    {
        heap_ = (uint8_t*)TAlloc::alloc( HEAP_START, HEAP_SIZE );
        param_ = (uint8_t*)TAlloc::alloc( PARAM_START, PARAM_SIZE );
        io_ = (uint8_t*)TAlloc::alloc( IO_START, IO_SIZE );
    }

    ~Memory()
    {
        TAlloc::free( heap_ );
        TAlloc::free( param_ );
        TAlloc::free( const_cast<void*>( reinterpret_cast<volatile void*>(io_)) );
    }

    uint32_t GetHeapSizeInBytes() const { return HEAP_SIZE; }
    
    template<typename T>
    T GetParam( uint32_t addr) const 
    { 
        TAlloc::on_read( PARAM_START + addr ); 
        return *(const T*)(param_+addr);
    }

    template<typename T>
    const T* GetParamPointer( uint32_t addr) const 
    { 
        TAlloc::on_read( PARAM_START + addr ); 
        return (const T*)(param_+addr);
    }    

    template<typename T>
    void SetHeap( uint32_t addr, T value )
    { 
        TAlloc::on_write( HEAP_START + addr, value ); 
        *(T*)( heap_ + addr ) = value; 
    }

    template<typename T>
    T GetHeap( uint32_t addr ) const 
    { 
        TAlloc::on_read( HEAP_START + addr ); 
        return *(const T*)(heap_+addr);
    }    

    template<typename T>
    T GetIO( uint32_t addr) const 
    { 
        TAlloc::on_read( IO_START + addr ); 
        return *(const volatile T*)(io_+addr);
    }    

    template<typename T>
    void SetIO( uint32_t addr, T value )
    { 
        TAlloc::on_write( IO_START + addr, value ); 
        *(volatile T*)( io_ + addr ) = value; 
    }

    void Sleep( int ticks )
    {
        for( int i = 0; i < ticks; ++i )
        {
            GetIO<uint32_t>(0);
        }
    }

private:
    uint8_t *heap_;
    uint8_t *param_;
    volatile uint8_t* io_;
};

}

#endif