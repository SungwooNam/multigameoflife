#ifndef __ARRAY_PROCESSOR_H__
#define __ARRAY_PROCESSOR_H__

typedef unsigned int    uint32_t;
typedef unsigned char   uint8_t;
typedef signed char     int8_t;
typedef unsigned short  uint16_t;
typedef short           int16_t;

enum MemoryRegion
{
    INSTRUCTION_START   = 0x000000,
    PARAM_START         = 0x100000,
    PARAM_SIZE          = 4096,
    HEAP_START          = 0x200000,
    HEAP_SIZE           = 4096,
    IO_START            = 0x300000,
    IO_SIZE             = 32,
};

enum ProcessorSpec
{
    PROCESSOR_COUNT = 8,
};

#endif

