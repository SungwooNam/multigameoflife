`include "testbench.v"

`timescale 1ns / 1ps

module tb_mem_i2c_1;
	`include "../src/asm.v"
	`include "../src/cmd.v"

    reg clk, rst;

    wire sda_o;
    wire scl;
    reg [15:0] addr;
    reg [31:0] wdata;
    reg wstrb;
    wire [31:0] rdata;
    reg rstrb;
    wire rbusy;

    reg [7:0] read_sda;
    reg sda_simul;

    mem_i2c #( 
        .SCL_PERIOD_CLOCKS(4),
        .WRITE_FIFO_DEPTH(256),
        .READ_FIFO_DEPTH(8)
        )
        utt( 
            .clk( clk),
            .rst_n( ~rst),
            
            .sda_i( sda_simul ),
            .sda_o( sda_o ),
            .scl( scl ),

            .addr( addr ),
            
            .wdata( wdata),
            .wstrb( wstrb ),
            .wbusy( wbusy ),
            
            .rdata( rdata ),
            .rstrb( rstrb ),
            .rbusy( rbusy )
    );

    initial begin
        rst = 1;
        addr = 0; wdata = 0; wstrb = 0; rstrb = 0; 
        sda_simul = 0;

        #10 rst = 0;
        @(posedge clk) #1 wstrb = 1; wdata = 32'h1142_CAFE; 
        @(posedge clk) #1 wstrb = 0;  
        @(negedge wbusy);        

    end

    initial begin
        #10;

        @(negedge sda_o) #1 sda_simul = 1; 
        read_sda_at_rising_scl( 8 );
        `EXPECT_EQ( read_sda[7:1], 7'h42);
        `EXPECT_EQ( read_sda[0], 1'h0 );
        @(negedge scl) #1 sda_simul = 0;
        read_sda_at_rising_scl( 1 );

        @(negedge scl) #1 sda_simul = 1; 
        read_sda_at_rising_scl( 8 );
        `EXPECT_EQ( read_sda[7:0], 8'hCA);
        @(negedge scl) #1 sda_simul = 0;
        read_sda_at_rising_scl( 2 );

    end


    initial begin
        #15 `EXPECT_EQ( wbusy, 0 );
        #10 `EXPECT_EQ( wbusy, 1 );
        #10 `EXPECT_EQ( wbusy, 1 );
        #10 `EXPECT_EQ( wbusy, 0 );
    end

    initial begin
        #15 `EXPECT_EQ( rdata, 32'h0101_0000 );
        #34 `EXPECT_EQ( rdata, 32'h0001_0000 );
        #95 `EXPECT_EQ( rdata, 32'h0101_0000 );
    end


    initial begin
        clk = 1'b1;
        #1000 $finish;
    end

    always clk = #5 ~clk;

    initial begin
        $dumpfile("tb_mem_i2c_1.vcd");
        $dumpvars(-1, tb_mem_i2c_1); 
    end

    `include "tb_i2c_tasks.v"

endmodule
