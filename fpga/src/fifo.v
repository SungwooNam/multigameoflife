module fifo #(
    parameter USE_BLOCK_RAM = 0,
    parameter DATA_WIDTH = 8,
    parameter FIFO_DEPTH = 8    //  2, 4, 8 ... 
    )(
    input                       clk,
    input                       rst_n,
    input                       wren_i,
    input                       rden_i,
    input   [DATA_WIDTH-1:0]    wdata_i,
    output  [DATA_WIDTH-1:0]    rdata_o,
    output                      full_o,
    output                      empty_o
    );
    
    localparam FIFO_DEPTH_LG2 = $clog2(FIFO_DEPTH);
    
    reg [FIFO_DEPTH_LG2:0] wrptr;
    reg [FIFO_DEPTH_LG2:0] rdptr;
    
    always @(posedge clk, negedge rst_n) begin
        if (!rst_n) begin
            wrptr <= {(FIFO_DEPTH_LG2+1){1'b0}};
        end else if (wren_i) begin
            wrptr <= wrptr + 'd1;
        end
    end
    
    always @(posedge clk, negedge rst_n) begin
        if (!rst_n) begin
            rdptr <= {(FIFO_DEPTH_LG2+1){1'b0}};
        end else if (rden_i) begin
            rdptr <= rdptr + 'd1;
        end
    end


    if ( USE_BLOCK_RAM == 1 ) begin
        reg [DATA_WIDTH-1:0] mem[0:FIFO_DEPTH-1];
        reg [FIFO_DEPTH_LG2-1:0] rdptr_reg;
        reg empty_reg;
        
        always @(posedge clk) begin
            empty_reg <= wrptr==rdptr;
            rdptr_reg <= rdptr[FIFO_DEPTH_LG2-1:0];
            if (wren_i) begin
                mem[wrptr[FIFO_DEPTH_LG2-1:0]] <= wdata_i;
            end
        end
        assign rdata_o = mem[rdptr_reg];
        assign empty_o = empty_reg;                
    end
    else begin
    
        reg [DATA_WIDTH-1:0] mem [0:FIFO_DEPTH-1];
        
        always @(posedge clk) begin
            if (wren_i) begin
                mem[wrptr[FIFO_DEPTH_LG2-1:0]] <= wdata_i;
            end
        end
        
        assign rdata_o = mem[rdptr[FIFO_DEPTH_LG2-1:0]];
        assign empty_o = (wrptr==rdptr);
    end

    assign full_o   =   (wrptr[FIFO_DEPTH_LG2-1:0]==rdptr[FIFO_DEPTH_LG2-1:0]) & 
                        (wrptr[FIFO_DEPTH_LG2] != rdptr[FIFO_DEPTH_LG2]);
    
endmodule