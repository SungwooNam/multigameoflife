#ifndef __FRAME_BUFFER_HPP__
#define __FRAME_BUFFER_HPP__

#include "Memory.hpp"

namespace RISCV {

template< typename TMemory = Memory<AllocBareMetal> >
class FrameBuffer
{
public:
    FrameBuffer( TMemory& mem, uint32_t base_addr, uint8_t width, uint8_t height ) 
        : mem_(mem), base_addr_(base_addr), width(width), height(height)
    {
    }

    uint8_t PageCount() const { return height/8; }
    uint32_t GetBaseAddr() const { return base_addr_; }

    void Clear( uint32_t fill )
    {
        for( int i = 0; i < width * PageCount(); i+=4 )
        {
            mem_.SetHeap( base_addr_+i, fill );
        }
    }

    void SetPixel( uint8_t x, uint8_t y, uint8_t v )
    {
        uint32_t addr = base_addr_ + x + ( y/8 )*width;
        uint8_t v8 = mem_.template GetHeap<uint8_t>( addr );
        uint8_t y_bit = 1 << (y&0x7);
        if( v != 0 ) 
        {
            mem_.SetHeap( addr, uint8_t(v8 | y_bit) );
        }
        else 
        {
            mem_.SetHeap( addr, uint8_t(v8 & (~y_bit)));
        }
    }

    uint8_t GetPixel( uint8_t x, uint8_t y )
    {
        uint32_t addr = base_addr_ + x + ( y/8 )*width;
        uint8_t v8 = mem_.template GetHeap<uint8_t>( addr );
        uint8_t y_bit = 1 << (y&0x7);
        return (( v8 & y_bit) !=0 ) ? 1 : 0; 
    }

    uint8_t GetNeighbor( uint8_t x, uint8_t y )
    {
        uint8_t x_p1 = x < width-1 ? x + 1 : 0;
        uint8_t x_m1 = x != 0 ? x-1 : width -1;
        uint8_t y_p1 = y < height-1 ? y + 1 : 0;
        uint8_t y_m1 = y != 0 ? y-1 : height -1;

        uint8_t count = 0;
        count += GetPixel( x_m1, y_p1 );
        count += GetPixel( x,    y_p1 );
        count += GetPixel( x_p1, y_p1 );
        count += GetPixel( x_m1, y );
        count += GetPixel( x_p1, y );
        count += GetPixel( x_m1, y_m1 );
        count += GetPixel( x,    y_m1 );
        count += GetPixel( x_p1, y_m1 );

        return count;
    }

private:
    TMemory& mem_;
    uint32_t base_addr_;

public:
    uint8_t width, height;
};

}

#endif