`include "testbench.v"

`timescale 1ns / 1ps

module tb_ap_io_im_2;
	`include "../src/asm.v"
	`include "../src/cmd.v"

    reg clk, rst, sw_rst;

    reg [31:0] cmd_data;
    reg cmd_valid;
    wire cmd_ready;

    wire [31:0] resp_data;
    wire resp_valid;
    wire resp_last;
    reg resp_ready;

    wire [23:0] mem_addr;
    wire mem_wstrb;
    wire mem_wready;
    wire [31:0] mem_wdata;

    cmd_decoder #( )
        uut_cmd_decoder( 
            .clk(clk), .rst_n(~rst), 
            .rx_data( cmd_data ),
            .rx_valid( cmd_valid ),
            .rx_ready( cmd_ready ), 
            .tx_data( resp_data), 
            .tx_valid( resp_valid ), 
            .tx_last( resp_last ), 
            .tx_ready( resp_ready ),
            .mem_addr( mem_addr ),
            .mem_wstrb( mem_wstrb ),
            .mem_wready( mem_wready ),
            .mem_wdata( mem_wdata ) 
        );
            
    wire [15:0] io_addr;
    wire [31:0] io_wdata;
    wire io_wstrb;
    reg [31:0] io_rdata;
    wire io_rstrb;
    reg io_wbusy;
    reg io_rbusy;

    array_processor_io_im #(
            .PROCESSOR_COUNT(1),
            .INSTRUCTION_MEM_SIZE(4096),
            .PARAM_MEM_SIZE(32),
            .HEAP_MEM_SIZE(4096)
        )
        ap(
            .clk(clk), .rst(rst),
            .sw_rst(sw_rst),
            .prog_addr( mem_addr ),
            .prog_wdata( mem_wdata ),
            .prog_wstrb( mem_wstrb ),
            .prog_wready( mem_wready ),

            .io_addr_0( io_addr ),
            .io_wdata_0( io_wdata ),
            .io_wstrb_0( io_wstrb ),
            .io_rdata_0( io_rdata ),
            .io_rstrb_0( io_rstrb ),
            .io_wbusy_0( io_wbusy ),
            .io_rbusy_0( io_rbusy )
        );

/*

    void Command( const uint32_t* ctrls, int count ) { 
        for( int i = 0; i < count; ++i ) {
            mem_.SetIO( 0, 0x123c0000 | ctrls[i] );
        }
    }    

    Memory mem;
    SSD1306 sd( mem );
    uint8_t init[] = { 0x8d, 0x14, 0xa4, 0xaf};
    sd.Command( init, 4);


   0:   00201137                lui     sp,0x201
   4:   008000ef                jal     ra,c <main>

00000008 <_fini>:
   8:   0000006f                j       8 <_fini>

0000000c <main>:
   c:   ff010113                addi    sp,sp,-16 # 200ff0 <__global_pointer$+0x1ff784>
  10:   afa417b7                lui     a5,0xafa41
  14:   48d78793                addi    a5,a5,1165 # afa4148d <SP_START+0xaf84048d>
  18:   00f12623                sw      a5,12(sp)
  1c:   00c10793                addi    a5,sp,12
  20:   01010593                addi    a1,sp,16
  24:   123c0637                lui     a2,0x123c0
  28:   003006b7                lui     a3,0x300
  2c:   0007c703                lbu     a4,0(a5)
  30:   00c76733                or      a4,a4,a2
  34:   00e6a023                sw      a4,0(a3) # 300000 <SP_START+0xff000>
  38:   00178793                addi    a5,a5,1
  3c:   feb798e3                bne     a5,a1,2c <main+0x20>
  40:   0000006f                j       40 <main+0x34>
*/
    initial begin
        sw_rst = 0; rst = 1;
        io_wbusy = 0; io_rbusy = 0; io_rdata = 0;

        #10 rst = 0; resp_ready = 1; cmd_valid=0; 
        @(posedge clk);
        @(posedge clk) #1 cmd_valid=1; cmd_data=cmd_io( 0 );  
        @(posedge cmd_ready) #1 cmd_data=cmd_mem_write(24'h01_0000, 15);  
        @(posedge cmd_ready) #1 cmd_data=32'h00201137;  
        @(posedge cmd_ready) #1 cmd_data=32'h008000ef;  
        @(posedge cmd_ready) #1 cmd_data=32'h0000006f;  
        @(posedge cmd_ready) #1 cmd_data=32'hff010113;  
        @(posedge cmd_ready) #1 cmd_data=32'hafa417b7;  
        @(posedge cmd_ready) #1 cmd_data=32'h48d78793;  
        @(posedge cmd_ready) #1 cmd_data=32'h00f12623;  
        @(posedge cmd_ready) #1 cmd_data=32'h00c10793;  
        @(posedge cmd_ready) #1 cmd_data=32'h01010593;  
        @(posedge cmd_ready) #1 cmd_data=32'h123c0637;  
        @(posedge cmd_ready) #1 cmd_data=32'h003006b7;  
        @(posedge cmd_ready) #1 cmd_data=32'h0007c703;  
        @(posedge cmd_ready) #1 cmd_data=32'h00c76733;  
        @(posedge cmd_ready) #1 cmd_data=32'h00e6a023;  
        @(posedge cmd_ready) #1 cmd_data=32'h00178793;  
        @(posedge cmd_ready) #1 cmd_data=32'hfeb798e3;  
        @(posedge cmd_ready) #1 cmd_valid=0; 
        @(posedge clk); 
        @(posedge clk); sw_rst = 1;

        @(posedge io_wstrb); #1 io_wbusy = 1;
        @(posedge clk);        
        @(posedge clk);        
        @(posedge clk) #1 io_wbusy = 0;        
    end

    initial begin
        clk = 1'b1;

        #2000 $finish;
    end

    always clk = #5 ~clk;

    initial begin
        $dumpfile("tb_ap_io_im_2.vcd");
        $dumpvars(-1, tb_ap_io_im_2); 
    end

    initial fork
        #991  `EXPECT_EQ( io_wdata, 32'h123c_008d);
        #1181 `EXPECT_EQ( io_wdata, 32'h123c_0014);
        #1351 `EXPECT_EQ( io_wdata, 32'h123c_00a4);
        #1521 `EXPECT_EQ( io_wdata, 32'h123c_00af);
    join

endmodule
