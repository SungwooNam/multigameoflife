#ifndef __LINEAR_FEEDBACK_SHIFT_REGISTER_HPP
#define __LINEAR_FEEDBACK_SHIFT_REGISTER_HPP

#include "array_processor.h"

namespace RISCV{

class FibonacciLFSR
{
public:
    FibonacciLFSR( uint16_t seed ) : lfsr_(seed)
    {
    }

    uint16_t Random() 
    {
        uint32_t bit = ((lfsr_ >> 0) ^ (lfsr_ >> 2) ^ (lfsr_ >> 3) ^ (lfsr_ >> 5)) & 1u;
        lfsr_ = ((lfsr_ >> 1) | (bit << 15)) & 0xFFFF;
        return (uint16_t) lfsr_;
    }

private:
    uint32_t lfsr_;
};

class XORShift
{
public:
    XORShift( uint16_t seed ) : lfsr_(seed)
    {
    }

    uint16_t Random() 
    {
        lfsr_ ^= lfsr_ >> 7;
        lfsr_ ^= lfsr_ << 9;
        lfsr_ ^= lfsr_ >> 13;
        return lfsr_;
    }

private:
    uint16_t lfsr_;
};



}

#endif