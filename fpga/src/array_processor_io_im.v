`timescale 1ns / 1ps

/*
rv32im

---
memory region
---
00_0000 - 0F_FFFF : programming instructions
    00_0000 - 00_FFFF : processor 0 instructions
    01_0000 - 01_FFFF : processor 1 instructions
    ...
10_0000 - 1F_FFFF : programming parameters 
    10_0000 - 10_FFFF : processor 0 parameters 
    11_0000 - 11_FFFF : processor 1 parameters 
    ....
20_0000 - 2F_FFFF : heap for each processor
30_0000 - 3F_FFFF : IO for each processor
*/
 
module array_processor_io_im 
    #(
        parameter INIT_INSTRUCTION_RAM = "none",
        parameter INIT_PARAM_RAM = "none",
        parameter PROCESSOR_COUNT = 1,
        parameter DATA_WIDTH = 32,
        parameter ADDR_WIDTH = 24,
        parameter SEG_ADDR_WIDTH = 20,
        parameter PROCESSOR_ADDR_WIDTH = 16,
        parameter INSTRUCTION_MEM_SIZE = 64,
        parameter PARAM_MEM_SIZE = 32,
        parameter HEAP_MEM_SIZE = 32,
        parameter SINGLE_INSTRUCTIONS = 1
    )
    (
        input [ADDR_WIDTH-1:0] prog_addr,
        input [DATA_WIDTH-1:0] prog_wdata,
        input prog_wstrb,
        output prog_wready,
        output [DATA_WIDTH-1:0] prog_rdata,
        output prog_rbusy,

        output [PROCESSOR_ADDR_WIDTH-1:0] io_addr_0,
        output [DATA_WIDTH-1:0] io_wdata_0,
        output io_wstrb_0,
        input io_wbusy_0,
        input [DATA_WIDTH-1:0] io_rdata_0,
        output io_rstrb_0,
        input io_rbusy_0,
        
        output [PROCESSOR_ADDR_WIDTH-1:0] io_addr_1,
        output [DATA_WIDTH-1:0] io_wdata_1,
        output io_wstrb_1,
        input io_wbusy_1,
        input [DATA_WIDTH-1:0] io_rdata_1,
        output io_rstrb_1,
        input io_rbusy_1,

        output [PROCESSOR_ADDR_WIDTH-1:0] io_addr_2,
        output [DATA_WIDTH-1:0] io_wdata_2,
        output io_wstrb_2,
        input io_wbusy_2,
        input [DATA_WIDTH-1:0] io_rdata_2,
        output io_rstrb_2,
        input io_rbusy_2,

        output [PROCESSOR_ADDR_WIDTH-1:0] io_addr_3,
        output [DATA_WIDTH-1:0] io_wdata_3,
        output io_wstrb_3,
        input io_wbusy_3,
        input [DATA_WIDTH-1:0] io_rdata_3,
        output io_rstrb_3,
        input io_rbusy_3,

        input clk,
        input rst,
        input sw_rst
    );
            
    localparam
        SEG_INST = 4'h0
        ,SEG_PARAM = 4'h1
        ,SEG_HEAP = 4'h2
        ,SEG_IO = 4'h3
        ;

    wire [ADDR_WIDTH-SEG_ADDR_WIDTH-1:0] prog_segment;
    assign prog_segment = prog_addr[ADDR_WIDTH-1:SEG_ADDR_WIDTH];
    wire prog_inst_sel = prog_segment == SEG_INST ? 1 : 0;
    assign prog_wready = 1;

    wire prog_param_sel;
    assign prog_param_sel = prog_segment == SEG_PARAM ? 1: 0;

    wire [31:0] peek_rdata[PROCESSOR_COUNT-1:0];
    assign prog_rdata = peek_rdata[ prog_addr[ SEG_ADDR_WIDTH-1: SEG_ADDR_WIDTH-4] ];
    assign prog_rbusy = 0;

    genvar i;
    generate
        for ( i = 0; i < PROCESSOR_COUNT; i = i + 1) begin
            
            wire [3:0] segment;
            wire [31:0] mem_addr;
            wire [DATA_WIDTH-1:0] mem_rdata;
            wire mem_rbusy;
            wire mem_rstrb;
            wire [DATA_WIDTH-1:0] mem_wdata;
            wire [3:0] mem_wmask;
            wire mem_wbusy;
            wire heap_wbusy;
            wire heap_rbusy;

            wire [DATA_WIDTH-1:0] inst_rdata;
            wire [DATA_WIDTH-1:0] param_rdata;
            wire [DATA_WIDTH-1:0] heap_rdata;
            wire [DATA_WIDTH-1:0] io_rdata;
            wire [3:0] heap_wmask;
            wire heap_busy;

            wire [3:0] io_wmask;

            assign segment = mem_addr[23:20];
            assign mem_rdata = 
                segment == SEG_INST ? inst_rdata : 
                segment == SEG_PARAM ? param_rdata :
                segment == SEG_HEAP ? heap_rdata : 
                io_rdata;

            assign heap_wmask = segment == SEG_HEAP ? mem_wmask : 0;
            assign io_wmask = segment == SEG_IO ? mem_wmask : 0;

            wire processor_sel;
            assign processor_sel = prog_addr[SEG_ADDR_WIDTH-1:SEG_ADDR_WIDTH-4] == i ? 1 : 0;
            wire prog_processor_wstrb;
            if ( SINGLE_INSTRUCTIONS == 1'b0 ) 
                assign prog_processor_wstrb = prog_wstrb & prog_inst_sel & processor_sel;
            else
                assign prog_processor_wstrb = prog_wstrb & prog_inst_sel;

            wire [SEG_ADDR_WIDTH-4-1:0] param_addr = 
                !sw_rst ? prog_addr[SEG_ADDR_WIDTH-4-1:0] : mem_addr[SEG_ADDR_WIDTH-4-1:0];
            wire heap_wenable = heap_wmask == 4'hF ? 1 : 0;
            
            if ( i == 0 ) begin
                assign io_addr_0 = mem_addr[PROCESSOR_ADDR_WIDTH-1:0];
                assign io_wdata_0 = mem_wdata;
                assign io_wstrb_0 = io_wmask != 0 ? 1 : 0;
                assign io_rdata = io_rdata_0;
                assign io_rstrb_0 = segment == SEG_IO ? mem_rstrb : 0;

                assign mem_rbusy = 
                    segment == SEG_HEAP ? heap_rbusy : 
                    segment == SEG_IO ? io_rbusy_0 : 
                    0;

                assign mem_wbusy = 
                    segment == SEG_HEAP ? heap_wbusy : 
                    segment == SEG_IO ? io_wbusy_0 : 
                    0;

                bram_8bits #(
                        .INIT( "init_param_0.dat" ),
                        .MEM_SIZE_IN_BYTES(PARAM_MEM_SIZE),
                        .ADDR_WIDTH(PROCESSOR_ADDR_WIDTH) 
                    )
                    param_rom ( 
                        .clk( clk ), .rst(rst),
                        .addr( param_addr ), 
                        .wdata( prog_wdata ),
                        .wmask( (prog_wstrb & prog_param_sel & processor_sel) ? 4'hF : 4'h0 ),
                        .rdata( param_rdata ) 
                    );            
            end
            else if ( i == 1 ) begin
                assign io_addr_1 = mem_addr[PROCESSOR_ADDR_WIDTH-1:0];
                assign io_wdata_1 = mem_wdata;
                assign io_wstrb_1 = io_wmask != 0 ? 1 : 0;
                assign io_rdata = io_rdata_1;
                assign io_rstrb_1 = segment == SEG_IO ? mem_rstrb : 0;

                assign mem_rbusy = 
                    segment == SEG_HEAP ? heap_rbusy : 
                    segment == SEG_IO ? io_rbusy_1 : 
                    0;

                assign mem_wbusy = 
                    segment == SEG_HEAP ? heap_wbusy : 
                    segment == SEG_IO ? io_wbusy_1 : 
                    0;

                bram_8bits #(
                        .INIT( "init_param_1.dat" ),
                        .MEM_SIZE_IN_BYTES(PARAM_MEM_SIZE),
                        .ADDR_WIDTH(PROCESSOR_ADDR_WIDTH) 
                    )
                    param_rom ( 
                        .clk( clk ), .rst(rst),
                        .addr( param_addr ), 
                        .wdata( prog_wdata ),
                        .wmask( (prog_wstrb & prog_param_sel & processor_sel) ? 4'hF : 4'h0 ),
                        .rdata( param_rdata ) 
                    );            
            end
            else if ( i == 2 ) begin
                assign io_addr_2 = mem_addr[PROCESSOR_ADDR_WIDTH-1:0];
                assign io_wdata_2 = mem_wdata;
                assign io_wstrb_2 = io_wmask != 0 ? 1 : 0;
                assign io_rdata = io_rdata_2;
                assign io_rstrb_2 = segment == SEG_IO ? mem_rstrb : 0;

                assign mem_rbusy = 
                    segment == SEG_HEAP ? heap_rbusy : 
                    segment == SEG_IO ? io_rbusy_2 : 
                    0;

                assign mem_wbusy = 
                    segment == SEG_HEAP ? heap_wbusy : 
                    segment == SEG_IO ? io_wbusy_2 : 
                    0;

                bram_8bits #(
                        .INIT( "init_param_2.dat" ),
                        .MEM_SIZE_IN_BYTES(PARAM_MEM_SIZE),
                        .ADDR_WIDTH(PROCESSOR_ADDR_WIDTH) 
                    )
                    param_rom ( 
                        .clk( clk ), .rst(rst),
                        .addr( param_addr ), 
                        .wdata( prog_wdata ),
                        .wmask( (prog_wstrb & prog_param_sel & processor_sel) ? 4'hF : 4'h0 ),
                        .rdata( param_rdata ) 
                    );            
            end
            else if ( i == 3 ) begin
                assign io_addr_3 = mem_addr[PROCESSOR_ADDR_WIDTH-1:0];
                assign io_wdata_3 = mem_wdata;
                assign io_wstrb_3 = io_wmask != 0 ? 1 : 0;
                assign io_rdata = io_rdata_3;
                assign io_rstrb_3 = segment == SEG_IO ? mem_rstrb : 0;

                assign mem_rbusy = 
                    segment == SEG_HEAP ? heap_rbusy : 
                    segment == SEG_IO ? io_rbusy_3 : 
                    0;

                assign mem_wbusy = 
                    segment == SEG_HEAP ? heap_wbusy : 
                    segment == SEG_IO ? io_wbusy_3 : 
                    0;

                bram_8bits #(
                        .INIT( "init_param_3.dat" ),
                        .MEM_SIZE_IN_BYTES(PARAM_MEM_SIZE),
                        .ADDR_WIDTH(PROCESSOR_ADDR_WIDTH) 
                    )
                    param_rom ( 
                        .clk( clk ), .rst(rst),
                        .addr( param_addr ), 
                        .wdata( prog_wdata ),
                        .wmask( (prog_wstrb & prog_param_sel & processor_sel) ? 4'hF : 4'h0 ),
                        .rdata( param_rdata ) 
                    );            
            end


            bram_32bits #(
                    .INIT(INIT_INSTRUCTION_RAM),
                    .MEM_SIZE_IN_BYTES(INSTRUCTION_MEM_SIZE),
                    .ADDR_WIDTH(PROCESSOR_ADDR_WIDTH-2) 
                )
                inst_rom ( 
                    .clk( clk ),
                    .waddr( prog_addr[PROCESSOR_ADDR_WIDTH-1:2] ), 
                    .wdata( prog_wdata ),
                    .wenable( prog_processor_wstrb ),
                    .raddr( mem_addr[PROCESSOR_ADDR_WIDTH-1:2] ),
                    .rdata( inst_rdata ) 
                );

            bram_8bits #(
                .MEM_SIZE_IN_BYTES(HEAP_MEM_SIZE),
                .ADDR_WIDTH(PROCESSOR_ADDR_WIDTH) 
                )
                heap_mem( 
                    .clk(clk), .rst(rst), 
                    .addr( mem_addr[PROCESSOR_ADDR_WIDTH-1:0] ), 
                    .wdata( mem_wdata ), 
                    .wmask( heap_wmask ), 
                    .wbusy( heap_wbusy ),
                    .rdata( heap_rdata ), 
                    .rstrb( mem_rstrb ), 
                    .rbusy( heap_rbusy )
                );

            bram_32bits #(
                .MEM_SIZE_IN_BYTES(HEAP_MEM_SIZE),
                .ADDR_WIDTH(PROCESSOR_ADDR_WIDTH-2) 
                )
                heap_mem_clone(
                    .clk( clk ),
                    .waddr( mem_addr[PROCESSOR_ADDR_WIDTH-1:2] ), 
                    .wdata( mem_wdata ),
                    .wenable( heap_wenable ),
                    .raddr( prog_addr[PROCESSOR_ADDR_WIDTH-1:2] ),
                    .rdata( peek_rdata[i] ) 
                );    

        	femtorv32_electron risc( 
                .clk(clk), 
                .mem_addr( mem_addr ), 
                .mem_wdata(mem_wdata), 
                .mem_wmask(mem_wmask),
                .mem_rdata(mem_rdata), 
                .mem_rstrb(mem_rstrb), 
                .mem_rbusy(mem_rbusy), 
                .mem_wbusy(mem_wbusy), 
                .reset(sw_rst) );                
        end        
    endgenerate

endmodule
			
		