#
# execute this on a build path not source path otherwise the path will be populated with temporary files
#

setws .
#app remove femto_app

app create -name femto_app -hw ./minized_femto.xsa -proc ps7_cortexa9_0 -os standalone -lang c++ -template "Empty Application (C++)"
importsource -name femto_app -path ./src 
app build -name femto_app
