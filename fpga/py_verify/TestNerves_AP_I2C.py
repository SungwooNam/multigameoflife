import unittest
import subprocess
import sys

from CmdDecoderMinized import *
from RiscvCompiler import *

class Point :
    def __init__( self, x, y ) :
        self.x = x
        self.y = y

class TestNerves_AP_I2C( unittest.TestCase ) :

    def testGameOfLife_ParamUpdate(self):
        compiler = RiscvCompiler()
        cmd = CmdDecoderMinized() 

        # only param update
        cmd.cmd( 'push 40000002' )
        cmd.write( RISCV_AP.PARAM_START | RISCV_AP.CORE_INDEX*0, [ 0x0000CAFE ] )
        cmd.write( RISCV_AP.PARAM_START | RISCV_AP.CORE_INDEX*1, [ 0x0000DEAD ] )
        cmd.write( RISCV_AP.PARAM_START | RISCV_AP.CORE_INDEX*2, [ 0x0000BEEF ] )
        cmd.write( RISCV_AP.PARAM_START | RISCV_AP.CORE_INDEX*3, [ 0x0000ACE1 ] )
        cmd.cmd( 'push 40000003' )

    def testGameOfLife_PrintInstruction(self):
        compiler = RiscvCompiler()
        for v in compiler.get_opcodes('../../build_rv32i/bin/GameOfLife') :
            print( f'{v:08x}')

    def line_pixels( self, start, end ) :
        if abs( start.x - end.x) > abs( start.y - end.y ) :
            if start.x < end.x :
                return [ (x, start.y + (end.y - start.y) * (x - start.x) / (end.x - start.x)) for x in range(start.x, end.x) ]
            else :
                return [ (x, start.y + (end.y - start.y) * (x - start.x) / (end.x - start.x)) for x in range(end.x, start.x) ]
        else :
            if start.y < end.y :
                return [ (start.x + (end.x - start.x) * (y - start.y) / (end.y - start.y), y) for y in range(start.y, end.y) ]
            else :
                return [ (start.x + (end.x - start.x) * (y - start.y) / (end.y - start.y), y) for y in range(end.y, start.y) ]
            
    def pixels_to_words( self, start, end ) :
        pixels = self.line_pixels( start, end )
        return [ (int(x) << 8 | int(y) ) for (x,y) in pixels ]
    
    def pack_pixel_words( self, pixels ) :
        if len(pixels) % 2 != 0 : 
            pixels += [ pixels[-1]] 
        return [ (pixels[i] << 16 | pixels[i+1]) for i in range(0, len(pixels), 2) ]

    def line_words( self, start, end ) :
        return self.pack_pixel_words( self.pixels_to_words( start, end ) ) 

    def testGameOfLife_UpdateInstruction(self):
        compiler = RiscvCompiler()
        cmd = CmdDecoderMinized() 

        cmd.stop()

        cmd.write( RISCV_AP.PARAM_START | RISCV_AP.CORE_INDEX*0, [ 0x0000CAFE ] )
        cmd.write( RISCV_AP.PARAM_START | RISCV_AP.CORE_INDEX*1, 
            [ 0x000010000] 
            + self.line_words( Point(30,10), Point(30,60) )
            + self.line_words( Point(90,10), Point(90,60) )
            + [ 0x00000000 ]
        )
        cmd.write( RISCV_AP.PARAM_START | RISCV_AP.CORE_INDEX*2, 
            [ 0x000010000] 
            + self.line_words( Point(60,10), Point(60,50) )
            + [ 0x00000000 ]
        )
        cmd.write( RISCV_AP.PARAM_START | RISCV_AP.CORE_INDEX*3, 
            [ 0x000010000] 
            + self.line_words( Point(20,31), Point(100,31) )
            + [ 0x00000000 ]
        )

        cmd.run( compiler.get_opcodes('../../build_rv32i/bin/GameOfLife') )

        # for i in range(1000):
        #     for core in range(4) : 
        #         values = cmd.read( 0x200000 + core * 0x10000 + 2048, 1 )
        #         print( f'core : {core}, io state {values[0]:08x}' )

if __name__ == '__main__' :
    # unittest.main()

    test = TestNerves_AP_I2C()
    # test.testGameOfLife_ParamUpdate()
    # test.testGameOfLife_PrintInstruction()
    test.testGameOfLife_UpdateInstruction()
