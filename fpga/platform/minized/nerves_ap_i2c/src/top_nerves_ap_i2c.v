`timescale 1ns / 1ps

module top_nerves_ap_i2c
    #(  parameter integer PROCESSOR_COUNT = 4,
        parameter integer SINGLE_INSTRUCTIONS = 1,
        parameter integer HEAP_MEM_SIZE = 4096,
        parameter integer INSTRUCTION_MEM_SIZE = 4096,
        parameter integer PARAM_MEM_SIZE = 4096 
    )
    (                        
        input clk,
        input rst_n,

        (* X_INTERFACE_INFO = "xilinx.com:interface:axis:1.0 RX TDATA" *)
        input [31:0] rx_data,
        (* X_INTERFACE_INFO = "xilinx.com:interface:axis:1.0 RX TVALID" *)
        input rx_valid,
        (* X_INTERFACE_INFO = "xilinx.com:interface:axis:1.0 RX TREADY" *)
        output rx_ready,
        
        (* X_INTERFACE_INFO = "xilinx.com:interface:axis:1.0 TX TDATA" *)
        output [31:0] tx_data,
        (* X_INTERFACE_INFO = "xilinx.com:interface:axis:1.0 TX TVALID" *)
        output tx_valid,
        (* X_INTERFACE_INFO = "xilinx.com:interface:axis:1.0 TX TLAST" *)
        output tx_last,
        (* X_INTERFACE_INFO = "xilinx.com:interface:axis:1.0 TX TREADY" *)
        input tx_ready,

		inout sda0,
		output scl0,
		inout sda1,
		output scl1,
		inout sda2,
		output scl2,
		inout sda3,
		output scl3,

        output led_red,
        output led_green
    );

	wire [23:0] mem_addr;
	wire mem_wstrb;
	wire [31:0] mem_wdata;
	wire mem_wready;
	wire [31:0] mem_rdata;
	wire mem_rstrb;
	wire mem_rbusy;
	wire [15:0] cmd_io_out;

    assign led_red = cmd_io_out[1];
    assign led_green = cmd_io_out[0];

	cmd_decoder
		cd(
			.clk(clk),
			.rst_n( rst_n ),
		
			.rx_data( rx_data ),
			.rx_valid( rx_valid ),
			.rx_ready( rx_ready ),
		
			.tx_data( tx_data ),
			.tx_valid( tx_valid ),
			.tx_ready( tx_ready ),
			.tx_last( tx_last ),
        
			.mem_addr( mem_addr ),
			.mem_wstrb( mem_wstrb ),
			.mem_wdata( mem_wdata ),
			.mem_wready( mem_wready ),
			.mem_rdata( mem_rdata ),
			.mem_rstrb( mem_rstrb ),
			.mem_rbusy( mem_rbusy ),

			.io_out( cmd_io_out )
		);

    wire [15:0] io_addr0;
    wire [31:0] io_wdata0;
    wire io_wstrb0;
    wire [31:0] io_rdata0;
    wire io_rstrb0;
    wire io_wbusy0;
    wire io_rbusy0;

    wire [15:0] io_addr1;
    wire [31:0] io_wdata1;
    wire io_wstrb1;
    wire [31:0] io_rdata1;
    wire io_rstrb1;
    wire io_wbusy1;
    wire io_rbusy1;

    wire [15:0] io_addr2;
    wire [31:0] io_wdata2;
    wire io_wstrb2;
    wire [31:0] io_rdata2;
    wire io_rstrb2;
    wire io_wbusy2;
    wire io_rbusy2;

    wire [15:0] io_addr3;
    wire [31:0] io_wdata3;
    wire io_wstrb3;
    wire [31:0] io_rdata3;
    wire io_rstrb3;
    wire io_wbusy3;
    wire io_rbusy3;

    wire delay_reset_n;
    delay_wire #(
            .DELAY(8'hFF),
            .DUTY(8'hFF),
            .ACTIVE(0)
        )
        delay_sw_reset_(
            .clk( clk),
            .rst_n( rst_n ),
            .delayed( delay_reset_n )
        );

    assign sw_reset_n = (cmd_io_out[1] == 1) ? cmd_io_out[0] : delay_reset_n;
    
	array_processor_io_im #(
            .INIT_INSTRUCTION_RAM("init_instruction.dat"),
			.PROCESSOR_COUNT(PROCESSOR_COUNT),
			.SINGLE_INSTRUCTIONS(SINGLE_INSTRUCTIONS),
			.HEAP_MEM_SIZE(HEAP_MEM_SIZE),
			.INSTRUCTION_MEM_SIZE(INSTRUCTION_MEM_SIZE),
			.PARAM_MEM_SIZE(PARAM_MEM_SIZE)
		)
		ap(
			.clk(clk), 
			.rst(~rst_n ),
			.sw_rst(sw_reset_n),
			
			.prog_addr( mem_addr ),
			.prog_wdata( mem_wdata ),
			.prog_wstrb( mem_wstrb ),
			.prog_wready( mem_wready ),
			.prog_rdata( mem_rdata ),
			.prog_rbusy( mem_rbusy ),

            .io_addr_0( io_addr0 ),
            .io_wdata_0( io_wdata0 ),
            .io_wstrb_0( io_wstrb0 ),
            .io_rdata_0( io_rdata0 ),
            .io_rstrb_0( io_rstrb0 ),
            .io_wbusy_0( io_wbusy0 ),
            .io_rbusy_0( io_rbusy0 ),

            .io_addr_1( io_addr1 ),
            .io_wdata_1( io_wdata1 ),
            .io_wstrb_1( io_wstrb1 ),
            .io_rdata_1( io_rdata1 ),
            .io_rstrb_1( io_rstrb1 ),
            .io_wbusy_1( io_wbusy1 ),
            .io_rbusy_1( io_rbusy1 ),

            .io_addr_2( io_addr2 ),
            .io_wdata_2( io_wdata2 ),
            .io_wstrb_2( io_wstrb2 ),
            .io_rdata_2( io_rdata2 ),
            .io_rstrb_2( io_rstrb2 ),
            .io_wbusy_2( io_wbusy2 ),
            .io_rbusy_2( io_rbusy2 ),

            .io_addr_3( io_addr3 ),
            .io_wdata_3( io_wdata3 ),
            .io_wstrb_3( io_wstrb3 ),
            .io_rdata_3( io_rdata3 ),
            .io_rstrb_3( io_rstrb3 ),
            .io_wbusy_3( io_wbusy3 ),
            .io_rbusy_3( io_rbusy3 )
		);        


    wire sda0_i, sda0_o, sda0_t;
    IOBUF sda0_iobuf ( .O(sda0_i), .I(sda0_o), .IO(sda0), .T(sda0_t) );

    mem_i2c #(
        .SCL_PERIOD_CLOCKS(1024),
        .WRITE_FIFO_DEPTH(256),
        .USE_BLOCK_RAM_FIFO(1),
        .READ_FIFO_DEPTH(8)
    )
        mi2c0(
            .clk(clk),
            .rst_n(sw_reset_n),

            .sda_i( sda0_i),
            .sda_o( sda0_o),
            .sda_t( sda0_t),
            .scl( scl0 ),

            .addr( io_addr0 ),
            
            .wdata( io_wdata0 ),
            .wstrb( io_wstrb0 ),
            .wbusy( io_wbusy0 ),

            .rdata( io_rdata0 ),
            .rstrb( io_rstrb0 ),
            .rbusy( io_rbusy0 )
        );

    wire sda1_i, sda1_o, sda1_t;
    IOBUF sda1_iobuf ( .O(sda1_i), .I(sda1_o), .IO(sda1), .T(sda1_t) );

    mem_i2c #(
        .SCL_PERIOD_CLOCKS(1024),
        .WRITE_FIFO_DEPTH(256),
        .USE_BLOCK_RAM_FIFO(1),
        .READ_FIFO_DEPTH(8)
    )
        mi2c1(
            .clk(clk),
            .rst_n(sw_reset_n),

            .sda_i( sda1_i),
            .sda_o( sda1_o),
            .sda_t( sda1_t),
            .scl( scl1 ),

            .addr( io_addr1 ),
            
            .wdata( io_wdata1 ),
            .wstrb( io_wstrb1 ),
            .wbusy( io_wbusy1 ),

            .rdata( io_rdata1 ),
            .rstrb( io_rstrb1 ),
            .rbusy( io_rbusy1 )
        );

    wire sda2_i, sda2_o, sda2_t;
    IOBUF sda2_iobuf ( .O(sda2_i), .I(sda2_o), .IO(sda2), .T(sda2_t) );

    mem_i2c #(
        .SCL_PERIOD_CLOCKS(1024),
        .WRITE_FIFO_DEPTH(256),
        .USE_BLOCK_RAM_FIFO(0),
        .READ_FIFO_DEPTH(8)
    )
        mi2c2(
            .clk(clk),
            .rst_n(sw_reset_n),

            .sda_i( sda2_i),
            .sda_o( sda2_o),
            .sda_t( sda2_t),
            .scl( scl2 ),

            .addr( io_addr2 ),
            
            .wdata( io_wdata2 ),
            .wstrb( io_wstrb2 ),
            .wbusy( io_wbusy2 ),

            .rdata( io_rdata2 ),
            .rstrb( io_rstrb2 ),
            .rbusy( io_rbusy2 )
        );

    wire sda3_i, sda3_o, sda3_t;
    IOBUF sda3_iobuf ( .O(sda3_i), .I(sda3_o), .IO(sda3), .T(sda3_t) );

    mem_i2c #(
        .SCL_PERIOD_CLOCKS(1024),
        .WRITE_FIFO_DEPTH(256),
        .USE_BLOCK_RAM_FIFO(0),
        .READ_FIFO_DEPTH(8)
    )
        mi2c3(
            .clk(clk),
            .rst_n(sw_reset_n),

            .sda_i( sda3_i),
            .sda_o( sda3_o),
            .sda_t( sda3_t),
            .scl( scl3 ),

            .addr( io_addr3 ),
            
            .wdata( io_wdata3 ),
            .wstrb( io_wstrb3 ),
            .wbusy( io_wbusy3 ),

            .rdata( io_rdata3 ),
            .rstrb( io_rstrb3 ),
            .rbusy( io_rbusy3 )
        );

endmodule
