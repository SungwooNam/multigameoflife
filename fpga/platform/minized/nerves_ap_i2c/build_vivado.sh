#!/bin/bash
source /opt/Xilinx/Vivado/2020.2/settings64.sh
PATH=$PATH:/opt/Xilinx/Vivado/2020.2/bin

BUILD_PATH=../../../../workspace_nerves_ap_i2c

mkdir -p $BUILD_PATH
rm -rf $BUILD_PATH/.Xil
rm -rf $BUILD_PATH/vivado_minized_nerves_ap_i2c*

cp -r -f ./src $BUILD_PATH
cp vivado_minized_nerves.tcl $BUILD_PATH
cp build_vivado.tcl $BUILD_PATH

cd $BUILD_PATH
vivado -mode tcl -source ./vivado_minized_nerves.tcl
vivado -mode tcl -source ./build_vivado.tcl
