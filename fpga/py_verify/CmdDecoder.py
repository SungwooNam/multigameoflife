from ast import parse
import typing
import time
import queue
import serial
import re
import time

class CmdDecoder() : 
    def __init__( self, devname="/dev/ttyUSB1" ) : 
        self.port = serial.Serial(
                port = devname,
                baudrate = 115200,
                bytesize = serial.EIGHTBITS,    
                parity = serial.PARITY_NONE,
                stopbits = serial.STOPBITS_ONE,
                timeout = 0.1
        )

    def cmd( self, data:str ) :
        self.port.write( data.encode())
        return self.read_response()

    def read_response( self ) -> str : 
        return ''

    # count in unit of 32bits
    def read( self, addr : int, count : int ) -> typing.List[ int ] :   
        return []

    def write( self, addr : int, values : typing.List[int] ) :   
        return []

    def run( self, opcodes : typing.List[int] ) :
        return []

    def stop( self ) :
        return
