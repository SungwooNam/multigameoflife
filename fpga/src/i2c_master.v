`timescale 1ns / 1ps

/*
// Pure Verilog
assign sda_i= sda_pin;
assign sda_pin = sda_t ? 1'bz : sda_o;


// Xilinx 
IOBUF sda_iobuf (
    .O(sda_i),   // 1-bit output: Buffer output
    .I(sda_o),   // 1-bit input: Buffer input
    .IO(sda_pin), // 1-bit inout: Buffer inout (connect directly to top-level port)
    .T(sda_t)    // 1-bit input: 3-state enable input
);
*/

module i2c_master 
    #(  
        parameter integer SCL_PERIOD_CLOCKS = 4,   // should be >= 4
		parameter integer DELAY_BITS_AT_STOP = 0
    )
    (
        input clk,
        input rst_n,

        (* MARK_DEBUG = "TRUE" *) input wire sda_i,
        (* MARK_DEBUG = "TRUE" *) output wire sda_o,
        output wire sda_t,

        (* MARK_DEBUG = "TRUE" *) output reg scl,

        output reg rden,
        input [15:0] rdata,
        input rempty,

        output reg wren,
        output reg [15:0] wdata,
        input wfull
    );

    parameter SCL_PERIOD = SCL_PERIOD_CLOCKS - 1;
    parameter SCL_PERIOD_HALF = SCL_PERIOD_CLOCKS >> 1;

    localparam 
        IDLE = 4'h0,
        DECODE_COMMAND = 4'h1,
        FIFO_CHECK = 4'h2,
        START = 4'h3,
        ADDRESS = 4'h4,
        ACK = 4'h5,
        NACK = 4'h6,
        WRITE = 4'h7,
        READ = 4'h8,
        STOP = 4'hE,
        PANIC = 4'hF
    ;

    localparam
        CMD_CONTROL = 4'h0,
        CMD_WRITE = 4'h1,
        CMD_READ = 4'h2
    ;

    localparam
        ACK_STATE_OK = 2'h0,
        ACK_STATE_NO_READ_ACK = 2'h2
    ;

    (* MARK_DEBUG = "TRUE" *) reg [3:0] state;
    reg [7:0] word_size;        // word is 8bits
    reg [7:0] addr;
    reg [3:0] cmd;
    reg [7:0] word_count;
    reg [3:0] bit_count;
    reg [$clog2(SCL_PERIOD):0] clock_count;
    reg [15:0] double_word;

    reg sda_drive;
    assign sda_t = sda_drive;
    assign sda_o = sda_drive;

    reg wf_wren;
    wire wf_wfull;

    reg wf_rden;
    wire [15:0] wf_rdata;
    wire wf_rempty;

    reg skip_first;

    fifo #( .DATA_WIDTH(16), .FIFO_DEPTH(16) )
        write_fifo(
            .clk( clk ),
            .rst_n( rst_n ),
            
            .wren_i( wf_wren),
            .wdata_i( rdata ),
            .full_o( wf_wfull ),

            .rden_i( wf_rden),
            .rdata_o( wf_rdata),
            .empty_o( wf_rempty)
        );

    always @( posedge clk ) begin
        if (!rst_n) begin
            state <= IDLE;
            word_size <= 0;
            word_count <= 0;
            bit_count <= 0;
            cmd <= CMD_CONTROL;
            rden <= 0;
            wf_wren <= 0;
            wf_rden <= 0;
            double_word <= 0;
            skip_first <= 0;
        end
        else begin

            case (state) 
                IDLE : begin
                    sda_drive <= 1;
                    scl <= 1;

                    if( !rempty ) begin
                        state <= DECODE_COMMAND;    // 1 more tick before reading rdata
                    end
                end

                DECODE_COMMAND : begin
                        case ( rdata[15:12] )
                            CMD_WRITE : begin 
                                addr[0] <= 0;
                                rden <= 1;
                                skip_first <= rdata[7];
                                state <= FIFO_CHECK;
                            end
                            CMD_READ : begin 
                                addr[0] <= 1;
                                rden <= 1;
                                state <= FIFO_CHECK;
                            end
                            CMD_CONTROL : begin 
                                state <= IDLE;
                            end
                        endcase                        

                        double_word <= rdata;
                        cmd <= rdata[15:12];
                        word_size <= rdata[11:8];
                        word_count <= 0;
                        addr[7:1] <= rdata[6:0];
                        bit_count <= 0;
                        clock_count <= 0;
                        double_word <= 0;
                    end

                FIFO_CHECK : begin

                    if( clock_count[1:0] == 2'b00 ) begin
                        rden <= 0;
                    end

                    if( cmd == CMD_WRITE ) begin
                        if( word_count < word_size ) begin
                            if( clock_count[1:0] == 2'b01 ) begin
                                if( !rempty ) begin
                                    wf_wren <= 1;
                                end
                            end
                            else if( clock_count[1:0] == 2'b10 && wf_wren == 1) begin
                                wf_wren <= 0;
                                rden <= 1;
                            end
                            else if( clock_count[1:0] == 2'b11 && rden == 1) begin
                                rden <= 0;
                                word_count <= word_count + 2;
                            end
                            clock_count <= clock_count + 1;
                        end
                        else begin
                            state <= START;
                            word_count <= 0;
                            clock_count <= 0;
                        end
                    end
                    else begin
                        state <= START;
						clock_count <= 0;
                    end
                end

                START : begin
                    sda_drive <= 0;

                    if( clock_count == SCL_PERIOD) begin
                        clock_count <= 0;
                    end
                    else begin
                        clock_count <= clock_count + 1;
                    end

                    if( clock_count == SCL_PERIOD_HALF) begin
                        scl <= 0;
                    end
                    else if( clock_count == SCL_PERIOD) begin
                        bit_count <= 0;
                        state <= ADDRESS;                    
                    end
                end

                ADDRESS : begin
                    if( clock_count == SCL_PERIOD) begin
                        clock_count <= 0;
                    end
                    else begin
                        clock_count <= clock_count + 1;
                    end

                    if( clock_count == 0 ) begin
                        scl <= 0;
                        sda_drive <= addr[7];
                        addr <= { addr[6:0], 1'b0 }; 
                    end
                    else if ( clock_count == SCL_PERIOD_HALF) begin
                        scl <= 1;
                    end
                    else if ( clock_count == SCL_PERIOD) begin
                        if( bit_count == 7 )  state <= ACK;
                        else bit_count <= bit_count + 1;
                    end
                end

                ACK : begin
                    if( clock_count == SCL_PERIOD) begin
                        clock_count <= 0;
                    end
                    else begin
                        clock_count <= clock_count + 1;
                    end

                    if( clock_count == 0 ) begin
                        scl <= 0;
                        sda_drive <= 1'b1;
                        wren <= 0;
                    end
                    else if ( clock_count == SCL_PERIOD_HALF) begin
                        scl <= 1;
                    end
                    else if ( clock_count == SCL_PERIOD) begin
                        if( sda_i != 1'b0 ) begin
                            state <= NACK;
                        end
                        else begin
							bit_count <= 0;
                            if( word_count == word_size ) begin
                                state <= STOP;
                            end 
                            else begin
                                case ( cmd )
                                    CMD_WRITE : begin
                                        state <= WRITE;
                                    end

                                    CMD_READ : begin
                                        state <= READ;
                                    end
                                    
                                    default : begin
                                        state <= PANIC;
                                    end
                                endcase
                            end 
                        end                                  
                    end
                end

                NACK : begin
                    if (word_count >= word_size ) begin
                        wren <= 0;
                        bit_count <= 0;
                        state <= STOP;
                    end
                    else begin
                        case ( cmd )
                            CMD_WRITE : begin
                                word_count <= word_count + 2;
                            end

                            CMD_READ : begin
                                if (wfull ) begin
                                    state <= PANIC; 
                                end
                                else begin
                                    wren <= 1;
                                    wdata <= {ACK_STATE_NO_READ_ACK, 14'h00};
                                    word_count <= word_count + 2;
                                end
                            end
                            
                            default : begin
                                state <= PANIC;
                            end
                        endcase
                    end
                end

                WRITE : begin
                    if( clock_count == SCL_PERIOD) begin
                        clock_count <= 0;
                    end
                    else begin
                        clock_count <= clock_count + 1;
                    end

                    if( clock_count == 0 ) begin
                        scl <= 0;
                        if( word_count[0]==0 && bit_count == 0) begin
                            wf_rden <= 1;
                            if( skip_first ) begin
                                double_word <= {wf_rdata[6:0], 9'b0};
                                sda_drive <= wf_rdata[7];
                                word_count <= 1;
                                skip_first <= 0;
                            end
                            else begin
                                double_word <= {wf_rdata[14:0], 1'b0};
                                sda_drive <= wf_rdata[15];
                            end
                        end
                        else begin
                            sda_drive <= double_word[15];
                            double_word <= {double_word[14:0], 1'b0};
                        end
                    end
                    else if( clock_count == 1 ) begin
                        wf_rden <= 0;
                    end
                    else if ( clock_count == SCL_PERIOD_HALF) begin
                        scl <= 1;
                    end
                    else if ( clock_count == SCL_PERIOD) begin
                        if( bit_count == 7 ) begin
                            state <= ACK;
                            word_count <= word_count + 1;
                        end

                        bit_count <= bit_count + 1;
                    end
                end

                READ : begin
                    if( clock_count == SCL_PERIOD) begin
                        clock_count <= 0;
                    end
                    else begin
                        clock_count <= clock_count + 1;
                    end
                    
                    if( clock_count == 0 ) begin
                        scl <= 0;
                        sda_drive <= 1'b1;
                        wren <= 0;
                    end
                    else if ( clock_count == SCL_PERIOD_HALF) begin
                        scl <= 1;
                        double_word <= {double_word[14:0], sda_i};
                        if( bit_count == 7) begin
                            word_count <= word_count + 1;
                        end
                    end
                    else if ( clock_count == SCL_PERIOD) begin
                        if( bit_count == 7 ) begin
                            if (word_count == word_size || word_count[0]==0) begin
                                if (wfull ) begin
                                    state <= PANIC; 
                                end
                                else begin
                                    if( word_count[0]==1) 
                                        wdata <= { double_word[7:0], 8'h0 }; 
                                    else
                                        wdata <= double_word;
                                    wren <= 1;
                                    double_word <= 16'h0000;
                                end
                            end
                            state <= ACK;
                        end

                        bit_count <= bit_count + 1;
                    end
                end                

                STOP : begin
                    if( clock_count == SCL_PERIOD) begin
                        clock_count <= 0;
                    end
                    else begin
                        clock_count <= clock_count + 1;
                    end

                    if( clock_count == 0 && bit_count == 0 ) begin
                        sda_drive <= 0;
                        scl <= 0;
                    end
                    else if ( clock_count == SCL_PERIOD_HALF && bit_count == 0 ) begin
                        scl <= 1;
                    end
                    else if ( clock_count == SCL_PERIOD) begin
                        sda_drive <= 1;
						
                        if( bit_count == DELAY_BITS_AT_STOP ) begin
							state <= IDLE;
						end
						else begin
							bit_count = bit_count + 1;
						end
                    end
                end     

                PANIC : begin

                end

                default : begin
                    state <= PANIC;
                end

            endcase
        end

    end


endmodule
