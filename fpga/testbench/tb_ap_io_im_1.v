`include "testbench.v"

`timescale 1ns / 1ps

module tb_ap_io_im_1;
	`include "../src/asm.v"
	`include "../src/cmd.v"

    reg clk, rst, sw_rst;

    reg [31:0] cmd_data;
    reg cmd_valid;
    wire cmd_ready;

    wire [31:0] resp_data;
    wire resp_valid;
    wire resp_last;
    reg resp_ready;

    wire [23:0] mem_addr;
    wire mem_wstrb;
    wire mem_wready;
    wire [31:0] mem_wdata;

    cmd_decoder #( )
        uut_cmd_decoder( 
            .clk(clk), .rst_n(~rst), 
            .rx_data( cmd_data ),
            .rx_valid( cmd_valid ),
            .rx_ready( cmd_ready ), 
            .tx_data( resp_data), 
            .tx_valid( resp_valid ), 
            .tx_last( resp_last ), 
            .tx_ready( resp_ready ),
            .mem_addr( mem_addr ),
            .mem_wstrb( mem_wstrb ),
            .mem_wready( mem_wready ),
            .mem_wdata( mem_wdata ) 
        );
            
    wire [15:0] io_addr;
    wire [31:0] io_wdata;
    wire io_wstrb;
    reg [31:0] io_rdata;
    wire io_rstrb;
    reg io_wbusy;
    reg io_rbusy;

    array_processor_io_im #(
            .PROCESSOR_COUNT(1)
        )
        ap(
            .clk(clk), .rst(rst),
            .sw_rst(sw_rst),
            .prog_addr( mem_addr ),
            .prog_wdata( mem_wdata ),
            .prog_wstrb( mem_wstrb ),
            .prog_wready( mem_wready ),

            .io_addr_0( io_addr ),
            .io_wdata_0( io_wdata ),
            .io_wstrb_0( io_wstrb ),
            .io_rdata_0( io_rdata ),
            .io_rstrb_0( io_rstrb ),
            .io_wbusy_0( io_wbusy ),
            .io_rbusy_0( io_rbusy )
        );

/*
Memory mem;
mem.SetIO( 0, 0xCAFEBEEF ); 
mem.SetIO( 2, mem.GetIO(1) );

003007b7                lui     a5,0x300
cafec737                lui     a4,0xcafec
eef70713                addi    a4,a4,-273 # cafebeef <SP_START+0xcaddbeef>
00e7a023                sw      a4,0(a5) # 300000 <SP_START+0xf0000>
0047a703                lw      a4,4(a5)
00e7a423                sw      a4,8(a5)
0000006f                j       24 <main+0x18>
*/
    initial begin
        sw_rst = 0; rst = 1;
        io_wbusy = 0; io_rbusy = 0; io_rdata = 0;

        #10 rst = 0; resp_ready = 1; cmd_valid=0; 
        @(posedge clk);
        @(posedge clk) #1 cmd_valid=1; cmd_data=cmd_io( 0 );  
        @(posedge cmd_ready) #1 cmd_data=cmd_mem_write(24'h01_0000, 6);  
        @(posedge cmd_ready) #1 cmd_data=32'h003007b7;  
        @(posedge cmd_ready) #1 cmd_data=32'hcafec737;  
        @(posedge cmd_ready) #1 cmd_data=32'heef70713;  
        @(posedge cmd_ready) #1 cmd_data=32'h00e7a023;  
        @(posedge cmd_ready) #1 cmd_data=32'h0047a703;  
        @(posedge cmd_ready) #1 cmd_data=32'h00e7a423;  
        @(posedge cmd_ready) #1 cmd_data=asm_loop( 0 );
        @(posedge cmd_ready) #1 cmd_valid=0; 
        @(posedge clk); 
        @(posedge clk); sw_rst = 1;

        @(posedge io_wstrb); #1 io_wbusy = 1;
        @(posedge clk);        
        @(posedge clk);        
        @(posedge clk) #1 io_wbusy = 0;        
    end

    initial begin
        // Below causes iVerilog wrongly simulate io_rstrb. Used absolute time #490 instead
        // @(posedge io_rstrb); #1 io_rdata = 32'h1234_5678; io_rbusy = 1;
        
        #490;
        #1  io_rdata = 32'h1234_5678; io_rbusy = 1;
        @(posedge clk);        
        @(posedge clk) #1 io_rbusy = 0;        
    end

    initial begin
        clk = 1'b1;

        #1000 $finish;
    end

    always clk = #5 ~clk;

    initial begin
        $dumpfile("tb_ap_io_im_1.vcd");
        $dumpvars(-1, tb_ap_io_im_1); 
    end

    initial fork
        #425 `EXPECT_EQ( io_wstrb, 0);
        #435 begin
            `EXPECT_EQ( io_addr, 16'h0000);
            `EXPECT_EQ( io_wdata, 32'hCAFE_BEEF);
            `EXPECT_EQ( io_wstrb, 1);
        end
        
        #485 `EXPECT_EQ( io_wstrb, 0);
        #495 begin
            `EXPECT_EQ( io_addr, 16'h0004);
            `EXPECT_EQ( io_rstrb, 1);
        end

        #535 `EXPECT_EQ( io_wstrb, 0);
        #545 begin
            `EXPECT_EQ( io_addr, 16'h0008);
            `EXPECT_EQ( io_wdata, 32'h1234_5678);
            `EXPECT_EQ( io_wstrb, 1);
        end        
    join

endmodule
