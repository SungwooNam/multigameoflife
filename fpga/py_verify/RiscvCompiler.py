from asyncio.subprocess import PIPE
import subprocess
import typing
import re

class RiscvCompiler() : 
    def __init__( self, bin_path = "/opt/rv32i/bin" ) :
        self.bin_path = bin_path

    def get_opcodes( self, path : str ) -> typing.List[ int ] :
        opcodes = None 

        with subprocess.Popen(f"{self.bin_path}/riscv32-unknown-elf-objdump -d {path} ", stdout=PIPE, shell=True) as proc:
            lines = proc.stdout.read()        

            values = []
            for line in lines.splitlines() :
                line = str( line, 'utf-8') 
                if re.search( '[0-9a-f]:', line ) and not re.search('file',line) : 
                    tokens = line.split( '\t' ) 
                    values += [ tokens[1] ] 

        return [ int( v, base=16 )  for v in values ]



if __name__ == '__main__' :
    compiler = RiscvCompiler()
    codes = compiler.get_opcodes('../../build_rv32i/bin/test2')
    print( f"{codes}")

