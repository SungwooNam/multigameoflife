from ast import parse
import typing
import time
import queue
import serial
import re
import time
import math
import enum

from CmdDecoder import *

class RISCV_AP() :
    INSTRUCTION_START   = 0x000000
    PARAM_START         = 0x100000
    HEAD_START          = 0x200000
    IO_START            = 0x300000
    CORE_INDEX          = 0x010000


class CmdDecoderMinized( CmdDecoder ) : 
    def __init__( self, devname="/dev/ttyUSB1" ) : 
        self.print_comm = False
        super(CmdDecoderMinized, self).__init__( devname )

    @property
    def print_comm(self):
        return self._print_comm

    @print_comm.setter
    def print_comm(self, flag):
        self._print_comm = flag 

    def cmd( self, data:str ) :
        self.port.write( data.encode() + b'\x0d' )
        if self.print_comm : print( data ) 
        return self.read_response()

    def read_response( self ) -> str : 
        response = ''
        while True : 
            for line in self.port.readlines() :
                line = str( line, 'utf-8')
                if line.find( '> ') != -1 :
                    if self.print_comm : print( response ) 
                    return response
                elif line.find( 'error' ) != -1 : 
                    raise Exception( f'failed with error : {line}' )
                response += line

    # count in unit of 32bits
    def read( self, addr : int, count : int ) -> typing.List[ int ] :   
        if count > 16 :
            raise Exception( "cannot read more than 16 elements" )

        self.cmd( f'push 2{(count-1):x}{addr:06x}' )
        response = self.cmd( 'pop' )

        return [ int( token, base=16 ) for token in re.split( '[ \r\n]', response ) if token ]

    def write( self, addr : int, values : typing.List[int] ) :   
        count = len(values)
        if count > 16 :
            raise Exception( "cannot write more than 16 elements" )

        self.cmd( f'push 3{(count-1):x}{addr:06x}' )
        self.cmd( 'push ' + ' '.join( [ format(v,'08x') for v in values ] ) )

    def run( self, opcodes : typing.List[int] ) :
        remained = len(opcodes)
        while remained > 0 : 
            start = len(opcodes) - remained
            count = min( remained, 16 )
                       
            self.cmd( f'push 3{(count-1):x}00{start*4:04x}' )
            push_opcode = 'push ' + ' '.join( [f'{v:08x}' for v in opcodes[start:start+count] ] )
            self.cmd( push_opcode )
            remained -= count
            print( f"{remained}" )

        self.cmd( 'push 40000003' )

    def stop( self ) :
        self.cmd( 'push 40000002' )

    def push( self, id : int, values : typing.List[int] ) : 
        count = len(values)
        if count > 0xFF :
            raise Exception( "cannot push more than 0xFF elements" )
        
        word_count = len(values)
        msg = f'push 600{id:1x}00{word_count:02x}'
        for i in range(len(values)) : 
            if i%2 == 0 : 
                msg += ' '
            msg += f'{values[i]:04x}'

        if len(values)%2 != 0 : msg += '0000'

        self.cmd( msg )

    def pop( self, id : int, count : int ) : 
        if count > 0xFF :
            raise Exception( "cannot pop more than 0xFF elements" )
        
        self.cmd( f'push 700{id:1x}00{count:02x}' )
        response = self.cmd( f'pop {math.ceil(count/2)}' )
        values = [ int( token, base=16 ) for token in re.split( '[ \r\n]', response ) if token ]
        
        ret = []
        for i in range(count) :
            if i%2 == 0 : 
                ret.append( values[(int)(i/2)] >> 16 )
            else :
                ret.append( values[(int)(i/2)] & 0xFFFF )
        return ret

            

if __name__ == '__main__' :
    # tokens = re.split( '[ \r\n]', '00000000 000001a5 0000004b 00000000 00000000 00000000 00000000 00000000')
    # print( tokens )

    # print( push( 1, [ 0xcafe, 0xbeef, 0x1234] ) )
    pass
