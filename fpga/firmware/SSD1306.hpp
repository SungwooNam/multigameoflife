/*
0x0000 : I2C WORD HI, LO
0x0004 : I2C FIFO status
    0[31:29] wfifo_full[28] 
    0[27:25] wfifo_empty[24]
    0[23:21] rfifo_full[20]
    0[19:17] rfifo_empty[16]
    0[15:0]
*/

#ifndef __SSD_1306_HPP__
#define __SSD_1306_HPP__

#include "Memory.hpp"
#include <algorithm>

namespace RISCV {

template< typename TMemory = Memory<AllocBareMetal> >
class SSD1306 
{
public:
    SSD1306( TMemory& mem ) : mem_(mem)
    {
    }

    void Command( const uint8_t* ctrls, int count ) 
    { 
        for( int i = 0; i < count; ++i ) {
            mem_.SetIO( 0, (uint32_t)(0x123c0000 | ctrls[i]) );
        }
    }

    void Data( const uint8_t* data, uint8_t count ) 
    {
        mem_.SetIO( 0, (uint32_t)( 0x10BC0040 | (count+2) << 24) );

        for( const uint8_t* p = data; p < data+count; p+=4 ) {
            mem_.SetIO( 0, (uint32_t)(p[0] << 24 | p[1] << 16 | p[2] <<8 | p[3]) );
        }
    }
    
    void DataStream( const uint8_t* data, int count, int sleep = 0 ) 
    {   
        int sent = 0;
        while( sent < count ) 
        {
            WaitForFIFOEmpty();
            mem_.Sleep(sleep);            

            uint8_t to_send = (uint8_t) std::min( 12, count - sent );
            Data( data + sent, to_send );
            sent += to_send;
        }
    }

    // IO Read adress 0x0004 is for simulation only. FPGA will read same value on all IO address range.
    uint8_t GetIO_Status() const
    { 
        auto v = mem_.template GetIO<uint32_t>(4);
        mem_.SetHeap(2048, v);
        return (uint8_t)( v >> 24);
    }

    void WaitForFIFOEmpty() 
    {
        for( ;; ) {
            if( GetIO_Status() == 0x01 ) break;
        }
    }

    TMemory& mem_;
};

}

#endif