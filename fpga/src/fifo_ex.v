/*
USE_BLOCK_RAM = 0
    rdata_o is valid at empty_o is 0
    rdata_o is valid after 1 tick of rden_i 1

USE_BLOCK_RAM = 1
    rdata_o is valid after 1 tick of empty_o 0 
    rdata_o is valid after 2 tick of rden_i 1
*/

module fifo_ex #(
        parameter USE_BLOCK_RAM = 0,
        parameter DATA_WIDTH = 8,
        parameter FIFO_DEPTH = 8,    //  2, 4, 8 ...
        parameter ALMOST_BITS = 1    //  number of least significant bits not comparing for almost_full, almost_empty
    )(
        input                       clk,
        input                       rst_n,
        input                       wren_i,
        input                       rden_i,
        input   [DATA_WIDTH-1:0]    wdata_i,
        output  [DATA_WIDTH-1:0]    rdata_o,
        output                      full_o,
        output                      empty_o,

        output reg [$clog2(FIFO_DEPTH):0] count_o,
        output almost_full_o,
        output almost_empty_o
    );
    
    fifo #(
        .USE_BLOCK_RAM( USE_BLOCK_RAM ),
        .DATA_WIDTH( DATA_WIDTH ),
        .FIFO_DEPTH( FIFO_DEPTH )
    )
    fifo_i(
        .clk( clk ),
        .rst_n( rst_n ),
        .wren_i( wren_i ),
        .rden_i( rden_i ),
        .wdata_i( wdata_i ),
        .rdata_o( rdata_o ),
        .full_o( full_o )
    );

    assign empty_o = count_o == 0;
    assign almost_full_o = full_o ? 1 : &count_o[$clog2(FIFO_DEPTH)-1:ALMOST_BITS];
    assign almost_empty_o = full_o ? 0 : ~(|count_o[$clog2(FIFO_DEPTH)-1:ALMOST_BITS]); 

    always @(posedge clk, negedge rst_n) begin
        if (!rst_n) begin
            count_o <= 0;
        end 
        else begin

            if (wren_i==1 && rden_i==0) begin
                count_o <= count_o + 1;
            end
            else if (wren_i==0 && rden_i==1) begin
                count_o <= count_o - 1;
            end
        end 
    end

endmodule