open_project ./minized_nerves/minized_nerves.xpr

update_compile_order -fileset sources_1

reset_run synth_1
launch_runs synth_1
wait_on_run synth_1

open_run synth_1 -name synth_1
report_utilization -name utilization_1

reset_run impl_1
launch_runs impl_1
wait_on_run impl_1
launch_runs impl_1 -to_step write_bitstream
wait_on_run impl_1

file copy -force ./minized_nerves/minized_nerves.runs/impl_1/spinal_cord_wrapper.bit ./spinal_cord_wrapper.bit

write_hw_platform -fixed -include_bit -force -file ./minized_nerves.xsa

exit