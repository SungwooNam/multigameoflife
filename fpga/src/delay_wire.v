module delay_wire #(
        parameter DELAY = 8'hFF,
        parameter DUTY = 8'hFF,
        parameter ACTIVE = 1'b1
    )(
        input clk,
        input rst_n,
        output reg delayed  
    );

    reg [$clog2(DELAY):0] count;

    reg [1:0] strobe_start;

    always @(posedge clk ) begin
        if (!rst_n) begin
            count <= DELAY;
            delayed <= ~ACTIVE;
            strobe_start <= 0;
        end 
        else begin
            if( count == 0 ) begin
                if( strobe_start == 0 ) begin
                    strobe_start <= 1;
                    delayed <= ~delayed;
                    count <= DUTY;                    
                end
                else begin
                    if( strobe_start == 1 && DUTY != 0 ) begin
                        strobe_start <= 2;
                        delayed <= ~delayed;
                    end
                end
            end
            else begin
                count <= count - 1;
            end
        end 
    end
    
endmodule