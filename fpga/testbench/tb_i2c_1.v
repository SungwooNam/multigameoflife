`include "testbench.v"

`timescale 1ns / 1ps

module tb_i2c_1;
    reg clk, rst;

    wire sda_o;
    wire scl;
    wire rden;
    wire [15:0] rdata;
    wire rempty;
    reg [7:0] read_sda;
    reg sda_simul;

    i2c_master #( 
        .SCL_PERIOD_CLOCKS(8)
        )
        i2c( 
            .clk(clk), 
            .rst_n(~rst), 
            
            .sda_i( sda_simul ),
            .sda_o( sda_o ),
            .scl( scl ),

            .rden( rden ),
            .rdata( rdata ),
            .rempty( rempty )
        );

    reg wren;
    reg [15:0] wdata;
                    
    fifo_ex #(
        .DATA_WIDTH(16),
        .FIFO_DEPTH(16),
        .ALMOST_BITS(1)
    ) 
        cmd_fifo(
            .clk(clk), 
            .rst_n(~rst),

            .wren_i(wren),
            .wdata_i(wdata),

            .rden_i(rden),
            .rdata_o(rdata),
            .empty_o(rempty)            
        );

    initial begin
        wren=0; 

        #5 rst = 1'b1;
        #10 rst = 1'b0;

        @(posedge clk);
        @(posedge clk); #1 wren=1; wdata=16'h1142;
        @(posedge clk); #1 wren=1; wdata=16'hCAFE;
        @(posedge clk); #1 wren=0;

        @(posedge clk);
        @(posedge clk); #1 wren=1; wdata=16'h1243;
        @(posedge clk); #1 wren=1; wdata=16'hBEEF;
        @(posedge clk); #1 wren=0;

    end     


    initial begin
        clk = 1'b1;
        #8000 $finish;
    end

    always clk = #5 ~clk;

    initial begin
        $dumpfile("tb_i2c_1.vcd");
        $dumpvars(-1, tb_i2c_1); 
    end

    initial begin 
        @(negedge sda_o) #1 sda_simul = 1; 
        read_sda_at_rising_scl( 8 );
        `EXPECT_EQ( read_sda[7:1], 7'h42);
        `EXPECT_EQ( read_sda[0], 1'h0 );
        @(negedge scl) #1 sda_simul = 0;
        read_sda_at_rising_scl( 1 );

        @(negedge scl) #1 sda_simul = 1; 
        read_sda_at_rising_scl( 8 );
        `EXPECT_EQ( read_sda[7:0], 8'hCA);
        @(negedge scl) #1 sda_simul = 0;
        read_sda_at_rising_scl( 2 );

        @(negedge sda_o) #1 sda_simul = 1; 
        read_sda_at_rising_scl( 8 );
        `EXPECT_EQ( read_sda[7:1], 8'h43);
        @(negedge scl) #1 sda_simul = 0;
        read_sda_at_rising_scl( 1 );

        @(negedge scl) #1 sda_simul = 1; 
        read_sda_at_rising_scl( 8 );
        `EXPECT_EQ( read_sda[7:0], 8'hBE);
        @(negedge scl) #1 sda_simul = 0;
        read_sda_at_rising_scl( 1 );

        @(negedge scl) #1 sda_simul = 1; 
        read_sda_at_rising_scl( 8 );
        `EXPECT_EQ( read_sda[7:0], 8'hEF);
        @(negedge scl) #1 sda_simul = 0;
        read_sda_at_rising_scl( 2 );

    end

    `include "tb_i2c_tasks.v"

endmodule
