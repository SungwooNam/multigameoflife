set(CMAKE_SYSTEM_NAME Linux)
set(CMAKE_SYSTEM_VERSION RV32I)
set(CMAKE_C_COMPILER /opt/rv32i/bin/riscv32-unknown-elf-gcc)
set(CMAKE_CXX_COMPILER /opt/rv32i/bin/riscv32-unknown-elf-g++)

set(CMAKE_EXE_LINKER_FLAGS "${CMAKE_EXE_LINKER_FLAGS} -L/opt/rv32i/lib")

include_directories(BEFORE 
    /opt/rv32i/include
)
