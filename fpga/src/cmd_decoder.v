`timescale 1ns / 1ps

module cmd_decoder (
        
        (* X_INTERFACE_INFO = "xilinx.com:interface:axis:1.0 RX TDATA" *)
        input [31:0] rx_data,
        (* X_INTERFACE_INFO = "xilinx.com:interface:axis:1.0 RX TVALID" *)
        input rx_valid,
        (* X_INTERFACE_INFO = "xilinx.com:interface:axis:1.0 RX TREADY" *)
        output reg rx_ready,
        
        (* X_INTERFACE_INFO = "xilinx.com:interface:axis:1.0 TX TDATA" *)
        output reg [31:0] tx_data,
        (* X_INTERFACE_INFO = "xilinx.com:interface:axis:1.0 TX TVALID" *)
        output reg tx_valid,
        (* X_INTERFACE_INFO = "xilinx.com:interface:axis:1.0 TX TLAST" *)
        output reg tx_last,
        (* X_INTERFACE_INFO = "xilinx.com:interface:axis:1.0 TX TREADY" *)
        input tx_ready,

        output reg [23:0] mem_addr,
        output reg mem_wstrb,
        output reg [31:0] mem_wdata,
        input mem_wready,
        input [31:0] mem_rdata,
        output reg mem_rstrb,
        input mem_rbusy,

        output reg [15:0] io_out,

        output reg txfifo_wren,
        output reg [15:0] txfifo_data,
        input txfifo_almost_full, 

        output reg rxfifo_rden,
        input [15:0] rxfifo_data,
        input rxfifo_empty, 

        input clk,
        input rst_n
    );
    
    localparam 
        FETCH = 4'h0,
        LOAD = 4'h1,
        MEM_TX = 4'h2,
        MEM_WRITE = 4'h3,
        BUS_TX = 4'h4,
        BUS_RX = 4'h5,
        BUS_RX_SETTLE = 4'h6,
        BUS_RX_TO_HOST = 4'h7,
        DONE = 4'hE,
        PANIC = 4'hF;
        
    localparam
        CMD_NONE = 4'h0,
        CMD_MEM_READ = 4'h2,
        CMD_MEM_WRITE = 4'h3,
        CMD_IO = 4'h4,
        CMD_DELAY = 4'h5,
        CMD_BUS_WRITE = 4'h6,
        CMD_BUS_READ = 4'h7
    ;
        
    reg [3:0] state;
    reg [31:0] data;
    reg [7:0] clock;

    wire [3:0] cmd;
    assign cmd = data[31:28];

    reg [3:0] cur_cmd;

    reg [8:0] word_count;
    reg [8:0] word_size;
    reg [15:0] delay;
    reg [3:0] bus;
    reg bus_word_hi;
        
    always @( posedge clk or negedge rst_n ) begin
        if (!rst_n) begin
            state <= FETCH;
            rx_ready <= 1;
            tx_valid <= 0; 
            tx_last <= 1;
            clock <= 0;
            delay <= 0;
            mem_wstrb <= 0;
            mem_rstrb <= 0;
            data <= 32'hdead_dead;
            txfifo_wren <= 0;
            txfifo_data <= 16'h0000;
            bus_word_hi <= 0;
            tx_data <= 32'h0000_0000;
            io_out <= 16'h0000;
            rxfifo_rden <= 0;
            cur_cmd <= CMD_NONE;
        end
        else begin
            clock <= clock + 1;
            
            case (state)
                FETCH : begin
                    if (rx_valid) begin 
                        data <= rx_data;
                        rx_ready <= 0;
                        if( cur_cmd == CMD_MEM_WRITE && word_count != 0 ) begin
                            state <= MEM_WRITE;
                        end 
                        else if ( cur_cmd == CMD_BUS_WRITE && word_count != word_size ) begin
                            state <= BUS_TX;
                        end
                        else begin
                            state <= LOAD;
                        end
                    end 
                end
                
                LOAD : begin
                    cur_cmd <= cmd;

                    case (cmd)
                        CMD_MEM_READ : begin
                            word_count <= data[27:24]; 
                            mem_addr <= data[23:0];
                            mem_rstrb <= 1;
                            state <= MEM_TX;
                        end

                        CMD_MEM_WRITE : begin
                            word_count <= data[27:24] + 1; 
                            mem_addr <= data[23:0] - 4;
                            state <= DONE;
                        end

                        CMD_IO : begin
                            io_out <= data[15:0]; 
                            state <= DONE;
                        end

                        CMD_DELAY : begin
                            delay <= data[15:0]; 
                            state <= DONE;
                        end

                        CMD_BUS_WRITE : begin
                            word_count <= 0; 
                            word_size <= data[7:0];
                            bus <= data[19:16];
                            bus_word_hi <= 1;
                            rx_ready <= 1;
                            state <= FETCH;
                        end

                        CMD_BUS_READ : begin
                            word_count <= 0; 
                            word_size <= data[7:0]; 
                            bus <= data[19:16];
                            bus_word_hi <= 1;
                            tx_data <= 32'h0000_0000;
                            state <= BUS_RX;
                        end

                        default : begin
                            state <= PANIC;
                        end
                    endcase
                end
                
                MEM_TX : begin
                    if( tx_ready == 1 && mem_rbusy == 0 && mem_rstrb == 0) begin
                        tx_data <= mem_rdata;
                        tx_valid <= 1;

                        if( word_count != 0 ) begin
                            word_count <= word_count - 1;
                            mem_rstrb <= 1;
                            mem_addr <= mem_addr + 4;
                        end
                        else begin
                            state <= DONE;
                        end
                    end
                    else begin
                        mem_rstrb <= 0;
                        tx_valid <= 0;
                    end
                end

                MEM_WRITE : begin
                    if( mem_wready == 1 ) begin
                        mem_addr <= mem_addr + 4;
                        mem_wstrb <= 1;
                        mem_wdata <= data;
                        word_count <= word_count - 1;
                        state <= DONE;
                    end
                    else begin

                    end
                end

                BUS_TX : begin
                    if( !txfifo_almost_full ) begin
                        txfifo_wren <= 1; 
                        if( bus_word_hi ) txfifo_data <= data[31:16];
                        else txfifo_data <= data[15:0];

                        bus_word_hi <= !bus_word_hi;
                        word_count <= word_count + 1;

                        if( word_count == word_size-1 ) begin
                            cur_cmd <= CMD_NONE;
                            state <= DONE;
                        end
                        else if( bus_word_hi == 0 ) begin
                            state <= DONE;
                        end
                    end
                    else begin
                        txfifo_wren <= 0;
                    end 
                end

                BUS_RX : begin
                    if ( tx_valid ) begin
                        tx_valid <= 0;
                        tx_data <= 32'h0000_0000; 
                    end
                    else if( !rxfifo_empty ) begin
                        rxfifo_rden <= 1;

                        if( bus_word_hi ) 
                            tx_data[31:16] <= rxfifo_data;
                        else
                            tx_data[15:0] <= rxfifo_data;

                        bus_word_hi <= !bus_word_hi;
                        word_count <= word_count + 1;
                        
                        if( word_count == word_size-1 || !bus_word_hi ) begin
                            state <= BUS_RX_TO_HOST;
                        end
                        else begin
                            state <= BUS_RX_SETTLE;
                            delay <= 1;
                        end
                    end
                    else begin
                        rxfifo_rden <= 0;
                    end 
                end

                BUS_RX_SETTLE : begin
                    rxfifo_rden <= 0;
                    if( delay != 0 ) delay <= delay - 1;
                    else state <= BUS_RX;
                end
                
                BUS_RX_TO_HOST: begin
                    rxfifo_rden <= 0;
                    if( tx_ready == 1 ) begin
                        tx_valid <= 1;

                        if( word_count == word_size ) begin
                            state <= DONE;
                        end
                        else begin
                            state <= BUS_RX;
                        end
                    end
                end

                
                DONE : begin
                    if( delay != 0 ) begin
                        delay <= delay - 1;
                    end
                    else begin
                        rx_ready <= 1;
                        tx_valid <= 0;
                        mem_wstrb <= 0;
                        mem_rstrb <= 0;
                        txfifo_wren <= 0;
                        rxfifo_rden <= 0;
                        state <= FETCH;
                    end
                end
            endcase
        end        
    end
endmodule
			
		