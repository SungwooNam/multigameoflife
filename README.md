# Multi GameOfLife

Fancy on SOC ( System On Chip ) ? But doesn't want to buy off-the-shelf but wanna put your hand dirty and see what can be the limit on customization ?

Here is 4 RISC-V 32bit processors, memory and I2C modules all in a FPGA and run game of life compiled with GNU toolchain. This project can provide you all the stepping stones from bare metal to the truly embedded application with a FPGA.

YouTube :
* https://www.youtube.com/watch?v=mg2PSXpvJpg
* https://www.youtube.com/watch?v=o4g6MSrszbs


## Description

### Overview of FPGA modules

![overview_fpga.png](doc/overview_fpga.png)

First, kudos to the FemtoRV project (https://github.com/BrunoLevy/learn-fpga). Each extension of processor ( electron used here ) is a single Verilog file and it can easily added to your project, and it works flawlessly.

This project adds input/output capabilities, memory, I2C, an interface to the host, Verilog verification, Xilinx Vivado support, compiler support, CMake, and a GitLab pipeline.

I burned the code to the Zynq (Minized) platform, so all of the hardware descriptions are based on that platform. Well, I know, that Minized is already obsolete, but this work should be applicable to other Xilinx Zynq platform easily.

All of the memory is BlockRAM on the FPGA, and it is very small. Each instruction memory is 4kB, parameter memory is 4kB, and heap memory is 4kB. You can increase the memory size in units of 4kB, as each BlockRAM is 36kbits. This means that you can have a maximum of 1k instructions. However, if you need more, you can increase the size as long as it does not exceed the total BlockRAM size of 50.

The memory can be initialized with predefined values. A data file can be used to store 32-bit hexadecimal values, so that the memory can store instructions to be executed as soon as the processor starts, just like a ROM. Alternatively, you can upload instructions with the provided Python script and execute them, just like a RAM.

Parameter memory is used to store processor-specific information, such as the processor number. It can also be initialized with a data file or uploaded at runtime.

Heap memory is used to store temporary variables and the stack, so that C++ code can run just like it does on a normal PC. However, there is no support for 'new' or 'delete'. For debugging purposes, the heap memory is cloned so that the user can peek into it to see what is going on inside.

To control 128x64 display - SSD1306, mem i2c module is connected to the processor so that it can write a value on a IO memory region and it will drive the i2c bus. 


## Build 

### build at ubuntu 22.02
* install prerequisite
```
$ sudo apt-get install -y cmake build-essential git python3-pip libgtest-dev
```

* install riscv32 compiler
```
$ sudo wget https://github.com/stnolting/riscv-gcc-prebuilt/releases/download/rv32i-4.0.0/riscv32-unknown-elf.gcc-12.1.0.tar.gz
$ sudo mkdir /opt/rv32i &&\
$ sudo tar -xzf riscv32-unknown-elf.gcc-12.1.0.tar.gz -C /opt/rv32i/ &&\
```
* install Xilinx Vivao, Vitis 2020 from Xilinx
* build
```
$ cmake -S . -B ./build_x86_64 && cmake --build ./build_x86_64
$ cmake -S . -B ./build_rv32i -DRV32I=1 -DCMAKE_TOOLCHAIN_FILE=../toolchain_rv32i.cmake && cmake --build ./build_rv32i
$ cmake -S . -B ./build_minized_nerves_ap_i2c -DMINIZED_NERVES_AP_I2C=1 && cmake --build ./build_minized_nerves_ap_i2c
```


### build at docker
```
$ docker run --rm -it -v "$PWD":/du -w /du -u 0 samplecpp_build:0.4
# cmake -S . -B ./build && cmake --build ./build
# cmake -S . -B ./build_rv32i -DRV32I=1 -DCMAKE_TOOLCHAIN_FILE=../toolchain_rv32i.cmake && cmake --build ./build_rv32i
```

## Run

### Program device
* connect Minized board to PC via USB and run ./program.sh. 
* You might see it fail at first time but pass at second time.
```
alice@wonderland:~/multigameoflife/workspace_nerves_vitis_ap_i2c/sd_card$ ./program.sh
****** Xilinx Program Flash
...
INFO: [Xicom 50-44] Elapsed time = 10 sec.
Flash Operation Successful
```
* hit reset

### Program via python
* After first program, you can program using Python. Refer fpga/py_verify/TestNerves_AP_I2c.py
```
alice@wonderland:~/multigameoflife/fpga/py_verify$ python3 TestNerves_AP_I2C.py 
```


## License
MIT

