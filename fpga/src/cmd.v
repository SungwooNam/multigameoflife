
function [31:0] cmd_mem_write( input [23:0] addr, input [3:0] count_minus_1 ); 
    cmd_mem_write = { 4'h3, count_minus_1, addr };
endfunction

function [31:0] cmd_mem_read( input [23:0] addr, input [3:0] count_minus_1 ); 
    cmd_mem_read = { 4'h2, count_minus_1, addr };
endfunction

function [31:0] cmd_io( input [15:0] io_out ); 
    cmd_io = { 4'h4, 12'h0, io_out };
endfunction

function [31:0] cmd_delay( input [15:0] delay  ); 
    cmd_delay = { 4'h5, 12'h0, delay };
endfunction

function [31:0] cmd_bus_write( input [3:0] bus, input [7:0] word_count  ); 
    cmd_bus_write = { 4'h6, 8'h00, bus, 8'h00, word_count };
endfunction

function [31:0] cmd_bus_read( input [3:0] bus, input [7:0] word_count  ); 
    cmd_bus_read = { 4'h7, 8'h00, bus, 8'h00, word_count };
endfunction
