`timescale 1ns / 1ps

// block ram with 8bits per address but rdata is latched with 4byte aligned .e.g. addr 4 and addr 5 to 7 will emit same rdata
// read takes 1 clock. 
// write takes 1 clock if wmask is 0b1111, otherwise, 2 clock and wbusy will be 1.  
module bram_8bits
    #(  
        parameter INIT = "none",
        parameter integer MEM_SIZE_IN_BYTES = 64,    
        parameter integer ADDR_WIDTH = 16 )
    (
        input clk, 
        input rst,

        input [ADDR_WIDTH-1:0] addr,

        input [31:0] wdata,
        input [3:0] wmask,
        output reg wbusy,

        output [31:0] rdata,
        input rstrb,
        output rbusy
    );
    
    assign rbusy = 0;

    reg [31:0] rw_data;
    reg rw_enable;

    wire [ADDR_WIDTH-1:2] addr_32bits = addr[ADDR_WIDTH-1:2];
    wire [31:0] mem_wdata = !wbusy ? wdata : rw_data;
    wire mem_wenable = !wbusy ? wmask==4'hF : rw_enable;
    wire [31:0] rdata_aligned;
    assign rdata = rdata_aligned;

    localparam 
        STATE_IDLE = 2'h0,
        STATE_READ = 2'h1,
        STATE_WRITE = 2'h2
    ;
    
    reg [1:0] state;

    reg [3:0] wmask_cache;
    wire [31:0] bit_mask = { 
        wmask_cache[3] ? 8'hFF : 8'h00, 
        wmask_cache[2] ? 8'hFF : 8'h00, 
        wmask_cache[1] ? 8'hFF : 8'h00, 
        wmask_cache[0] ? 8'hFF : 8'h00
    };        

    always @( posedge clk ) begin
        if( rst == 1 ) begin 
            state <= STATE_IDLE;
            rw_data <= 0;
            rw_enable <= 0;
            wbusy <= 0;
        end
        else begin
            case(state)
                STATE_IDLE : begin
                    if( wmask != 0 && wmask != 4'hF) begin
                        wbusy <= 1;         
                        wmask_cache <= wmask;          
                        state <= STATE_READ;
                    end
                end
                STATE_READ : begin
                    rw_enable <= 1;
                    rw_data <= (rdata & ~bit_mask) | (wdata & bit_mask);
                    state <= STATE_WRITE;
                end
                STATE_WRITE : begin
                    rw_enable <= 0;
                    wbusy <= 0;
                    state <= STATE_IDLE;
                end
            endcase
        end
    end

    bram_32bits #(
        .INIT( INIT ),
        .MEM_SIZE_IN_BYTES(MEM_SIZE_IN_BYTES),
        .ADDR_WIDTH(ADDR_WIDTH-2) 
    )
    mem( 
        .clk(clk),
        .waddr(addr_32bits),
        .wdata(mem_wdata),
        .wenable(mem_wenable),
        .raddr(addr_32bits),
        .rdata(rdata_aligned)
    );

endmodule
