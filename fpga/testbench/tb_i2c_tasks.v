task read_sda_at_rising_scl( input [7:0] count);
    integer i;
    begin
        read_sda = 0;
        for( i=0; i<count; i = i +1 ) begin
            @(posedge scl) read_sda = { read_sda[30:0], sda_o };
        end
    end
endtask   

task write_sda_simul_at_falling_scl( input [7:0] value, input [7:0] count);
    integer i;
    begin
        for( i=0; i<count; i = i +1 ) begin
            @(negedge scl) sda_simul = value[7];
            value = { value[6:0], 1'b0 };
        end
    end
endtask