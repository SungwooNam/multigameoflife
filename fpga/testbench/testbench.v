`define EXPECT_EQ( signal, value ) \
     if( signal !== value) begin \
          $error( "%s is %x but should be %s", `"signal`", signal, `"value`" ); \
     end