`include "testbench.v"

`timescale 1ns / 1ps

module tb_i2c_2;
    reg clk, rst;

    wire sda_o;
    wire scl;
    wire cmd_rden;
    wire [15:0] cmd_rdata;
    wire cmd_rempty;
    wire rsp_wden;
    wire [15:0] rsp_wdata;
    wire rsp_wfull;
    wire rsp_rden;
    wire [15:0] rsp_rdata;
    wire rsp_rempty;
    reg [7:0] read_sda;
    reg sda_simul;

    i2c_master #( 
        .SCL_PERIOD_CLOCKS(8)
        )
        i2c( 
            .clk(clk), 
            .rst_n(~rst), 
            
            .sda_i( sda_simul ),
            .sda_o( sda_o ),
            .scl( scl ),

            .rden( cmd_rden ),
            .rdata( cmd_rdata ),
            .rempty( cmd_rempty ),

            .wren( rsp_wren ),
            .wdata( rsp_wdata ),
            .wfull( rsp_wfull )
        );

    reg cmd_wren;
    reg [15:0] cmd_wdata;
                    
    fifo_ex #(
        .DATA_WIDTH(16),
        .FIFO_DEPTH(16),
        .ALMOST_BITS(1)
    ) 
        cmd_fifo(
            .clk(clk), 
            .rst_n(~rst),

            .wren_i(cmd_wren),
            .wdata_i(cmd_wdata),

            .rden_i(cmd_rden),
            .rdata_o(cmd_rdata),
            .empty_o(cmd_rempty)
        );

    fifo_ex #(
        .DATA_WIDTH(16),
        .FIFO_DEPTH(8),
        .ALMOST_BITS(1)
    ) 
        rsp_fifo(
            .clk(clk), 
            .rst_n(~rst),

            .wren_i(rsp_wren),
            .wdata_i(rsp_wdata),
            .almost_full_o(rsp_wfull),

            .rden_i(rsp_rden),
            .rdata_o(rsp_rdata),
            .empty_o(rsp_rempty)
        );        

    initial begin
        cmd_wren=0; 

        #5 rst = 1'b1;
        #10 rst = 1'b0;

        @(posedge clk);
        @(posedge clk); #1 cmd_wren=1; cmd_wdata=16'h2142;
        @(posedge clk); #1 cmd_wren=0;

        @(posedge clk);
        @(posedge clk); #1 cmd_wren=1; cmd_wdata=16'h2243;
        @(posedge clk); #1 cmd_wren=0;

        @(posedge clk);
        @(posedge clk); #1 cmd_wren=1; cmd_wdata=16'h2344;
        @(posedge clk); #1 cmd_wren=0;
    end     


    initial begin
        clk = 1'b1;
        #8000 $finish;
    end

    always clk = #5 ~clk;

    initial begin
        $dumpfile("tb_i2c_2.vcd");
        $dumpvars(-1, tb_i2c_2); 
    end

    initial begin 
        @(negedge sda_o) #1 sda_simul = 1; 
        read_sda_at_rising_scl( 8 );
        `EXPECT_EQ( read_sda[7:1], 7'h42);
        `EXPECT_EQ( read_sda[0], 1'h1 );
        @(negedge scl) #1 sda_simul = 0;
        read_sda_at_rising_scl( 1 );

        write_sda_simul_at_falling_scl( 8'hCA, 8 );
        @(posedge rsp_wren) 
        `EXPECT_EQ( rsp_wdata, 16'hCA00 );
        @(negedge scl) #1 sda_simul = 0;
        read_sda_at_rising_scl( 2 );

        @(negedge sda_o) #1 sda_simul = 1; 
        read_sda_at_rising_scl( 8 );
        `EXPECT_EQ( read_sda[7:1], 7'h43);
        `EXPECT_EQ( read_sda[0], 1'h1 );
        @(negedge scl) #1 sda_simul = 0;
        read_sda_at_rising_scl( 1 );

        write_sda_simul_at_falling_scl( 8'hBE, 8 );
        @(negedge scl) #1 sda_simul = 0;
        read_sda_at_rising_scl( 1 );
        write_sda_simul_at_falling_scl( 8'hEF, 8 );
        @(posedge rsp_wren) 
        `EXPECT_EQ( rsp_wdata, 16'hBEEF );
        @(negedge scl) #1 sda_simul = 0;
        read_sda_at_rising_scl( 2 );

        @(negedge sda_o) #1 sda_simul = 1; 
        read_sda_at_rising_scl( 8 );
        `EXPECT_EQ( read_sda[7:1], 7'h44);
        `EXPECT_EQ( read_sda[0], 1'h1 );
        @(negedge scl) #1 sda_simul = 0;
        read_sda_at_rising_scl( 1 );

        write_sda_simul_at_falling_scl( 8'h12, 8 );
        @(negedge scl) #1 sda_simul = 0;
        read_sda_at_rising_scl( 1 );
        write_sda_simul_at_falling_scl( 8'h34, 8 );
        @(posedge rsp_wren) 
        `EXPECT_EQ( rsp_wdata, 16'h1234 );
        @(negedge scl) #1 sda_simul = 0;
        read_sda_at_rising_scl( 1 );

        write_sda_simul_at_falling_scl( 8'h56, 8 );
        @(posedge rsp_wren) 
        `EXPECT_EQ( rsp_wdata, 16'h5600 );
        @(negedge scl) #1 sda_simul = 0;
        read_sda_at_rising_scl( 2 );
    end

    `include "tb_i2c_tasks.v"

endmodule
