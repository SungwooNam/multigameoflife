#!/bin/bash
source /opt/Xilinx/Vivado/2020.2/settings64.sh
PATH=$PATH:/opt/Xilinx/Vitis/2020.2/bin

program_flash \
    -f ./BOOT.BIN \
    -offset 0 \
    -flash_type qspi-x4-single \
    -fsbl ./fsbl.elf \
    -cable type xilinx_tcf \
    url TCP:127.0.0.1:3121
