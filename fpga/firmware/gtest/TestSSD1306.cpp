#include "SSD1306.hpp"
#include "FrameBuffer.hpp"
#include "LinearFeedbackShiftRegister.hpp"
#include <string>
#include <sstream>
#include <map>
#include <gtest/gtest.h>
#include <gmock/gmock.h>
#include <iostream>

namespace RISCV::GTest {

struct AllocPeek {
    static void* alloc( uint32_t start, uint32_t sizeByte ) { 
		void *region = new uint8_t[sizeByte];
		peek_regions[ start ] = region;
		return region; 
	} 

    static void free( void *region ) { delete[] reinterpret_cast<uint8_t*>(region); } 

    static void on_read( uint32_t addr ) {}

    template<typename T> static void on_write( uint32_t addr, T value ) 
	{
		if constexpr ( std::is_integral_v<T> || std::is_floating_point_v<T> ) 
		{
			if(  MemoryRegion::IO_START <= addr && addr < (MemoryRegion::IO_START+MemoryRegion::IO_SIZE) )
			{
				peek_io.push_back( *(uint32_t*)(&value) );
			}
		}
		else {
			throw std::runtime_error( "Not supported");
		} 
	}

	static void clear() 
	{
		peek_regions.clear();
		peek_io.clear();
	}

	static std::map<uint32_t,void*> peek_regions; 
	static std::vector<uint32_t> peek_io;
};

std::map<uint32_t,void*> AllocPeek::peek_regions;
std::vector<uint32_t> AllocPeek::peek_io;


TEST( TestSSD1306, testCommand) 
{
	Memory<AllocPeek> mem;
	SSD1306 sd(mem);

	AllocPeek::clear();
    {
        uint8_t init[] = { 0x8d, 0x14, 0xa4, 0xaf, 0x20, 0x00, 0x20, 0x00 };
        sd.Command( init, 8);
	}

	EXPECT_THAT( AllocPeek::peek_io, testing::ElementsAre( 
		0x123c008d, 0x123c0014, 
		0x123c00a4,  
		0x123c00af, 
		0x123c0020, 0x123c0000, 
		0x123c0020, 0x123c0000
	));
} 

TEST( TestSSD1306, testData) 
{
	Memory<AllocPeek> mem;
	SSD1306 sd(mem);
	AllocPeek::clear();

	uint8_t pixels[] = { 0xab, 0xcd, 0x12, 0x34, 0x56, 0x78, 0xca, 0xfe };
 	sd.Data( pixels, 8);

	EXPECT_THAT( AllocPeek::peek_io, testing::ElementsAre( 
		0x1abc0040, 0xabcd1234, 0x5678cafe 
	));
} 

TEST( TestSSD1306, testDataStream) 
{
	AllocPeek::clear();

	Memory<AllocPeek> mem;
	SSD1306 sd(mem);

	mem.SetIO( 4, 0x01010000 );
	AllocPeek::peek_io.clear();
	ASSERT_EQ( sd.GetIO_Status(), 0x01 );

	std::vector<uint8_t> pixels( 16, 1 );
 	sd.DataStream( pixels.data(), 16);

	EXPECT_THAT( AllocPeek::peek_io, testing::ElementsAre( 
		0x1ebc0040, 0x01010101, 0x01010101, 0x01010101, 
		0x16bc0040, 0x01010101
	));
}

TEST( TestSSD1306, testDataStreamOnHeapAddress) 
{
	AllocPeek::clear();

	Memory<AllocPeek> mem;
	SSD1306 sd(mem);

	mem.SetIO( 4, 0x01010000 );
	AllocPeek::peek_io.clear();

	uint8_t *heap_region = (uint8_t*) AllocPeek::peek_regions[ 0x200000 ];
 	sd.DataStream( heap_region, 1024 );

	EXPECT_THAT( AllocPeek::peek_io.size(), (1024/12)*4 + (1024%12)/4+1 ); 
}

TEST( TestSSD1306, testFrameBuffer) 
{
	AllocPeek::clear();

	Memory<AllocPeek> mem;
	FrameBuffer fb( mem, 0, 128, 64 );
	fb.Clear(0x00000000);

	uint8_t *heap_region = (uint8_t*) AllocPeek::peek_regions[ 0x200000 ];
	for( int i = 0; i < 8; ++i )
	{
		fb.SetPixel( 42, i, 1 );
		EXPECT_EQ( heap_region[42], 1<<i );

		fb.SetPixel( 42, i, 0 );
		EXPECT_EQ( heap_region[42], 0 );	  
	}

	EXPECT_EQ( fb.GetPixel( 42, 24 ), 0 );
	fb.SetPixel( 42, 24, 1 );
	EXPECT_EQ( fb.GetPixel( 42, 24 ), 1 );

	EXPECT_EQ( fb.GetPixel( 42, 25 ), 0 );
	fb.SetPixel( 42, 25, 1 );
	EXPECT_EQ( fb.GetPixel( 42, 25 ), 1 );

	EXPECT_EQ( fb.GetPixel( 42, 26 ), 0 );
	fb.SetPixel( 42, 26, 1 );
	EXPECT_EQ( fb.GetPixel( 42, 26 ), 1 );

	EXPECT_EQ( fb.GetNeighbor( 43, 22 ), 0 );
	EXPECT_EQ( fb.GetNeighbor( 43, 23 ), 1 );
	EXPECT_EQ( fb.GetNeighbor( 43, 24 ), 2 );
	EXPECT_EQ( fb.GetNeighbor( 43, 25 ), 3 );
	EXPECT_EQ( fb.GetNeighbor( 43, 26 ), 2 );
	EXPECT_EQ( fb.GetNeighbor( 43, 27 ), 1 );
	EXPECT_EQ( fb.GetNeighbor( 43, 28 ), 0 );

	fb.SetPixel( 1, 0, 1 );
	fb.SetPixel( 1, 63, 1 );
	EXPECT_EQ( fb.GetNeighbor( 0, 0 ), 2 );
	EXPECT_EQ( fb.GetNeighbor( 0, 1 ), 1 );
	EXPECT_EQ( fb.GetNeighbor( 0, 63 ), 2 );
	EXPECT_EQ( fb.GetNeighbor( 0, 62 ), 1 );

	fb.SetPixel( 127, 63, 1 );
	EXPECT_EQ( fb.GetNeighbor( 0, 0 ), 3 );
}

TEST( TestSSD1306, testLFSR) 
{
	FibonacciLFSR r( 0xACE1 );

	int period = 0;
	uint16_t v;
	while( (v = r.Random()) != 0xACE1 ) { 
		period++;
	}
	EXPECT_GE( period, 0xFFF0 );
}

}
